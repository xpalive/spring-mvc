package com.webservice.model;

import java.util.Date;

public class XiaocaiUserApi {
	private long id;
    private String userPassword;
    private String userEmail;
    private String userQQ;
    private float userPoint;//用户可用金币
    private float frozenPoint;//用户冻结金币
    private float userMoney;//用户可用金额
    private float frozenMoney;//用户冻结金额
    private int userLevel;//用户级别
    private String userLevelName;//用户级别名称（0为普通用户，1为铜牌代理，2为银牌代理,3为金牌代理）
	private int banedIs;//帐号是否禁用
	private int warnedTimes;//帐号被警告次数
	private int referUserId;//推荐人ID
    private Date userRegDate ;//用户注册时间
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getUserPassword() {
		return userPassword;
	}
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public String getUserQQ() {
		return userQQ;
	}
	public void setUserQQ(String userQQ) {
		this.userQQ = userQQ;
	}
	public float getUserPoint() {
		return userPoint;
	}
	public void setUserPoint(float userPoint) {
		this.userPoint = userPoint;
	}
	public float getFrozenPoint() {
		return frozenPoint;
	}
	public void setFrozenPoint(float frozenPoint) {
		this.frozenPoint = frozenPoint;
	}
	public float getUserMoney() {
		return userMoney;
	}
	public void setUserMoney(float userMoney) {
		this.userMoney = userMoney;
	}
	public float getFrozenMoney() {
		return frozenMoney;
	}
	public void setFrozenMoney(float frozenMoney) {
		this.frozenMoney = frozenMoney;
	}
	public int getUserLevel() {
		return userLevel;
	}
	public void setUserLevel(int userLevel) {
		this.userLevel = userLevel;
	}
	public String getUserLevelName() {
		return userLevelName;
	}
	public void setUserLevelName(String userLevelName) {
		this.userLevelName = userLevelName;
	}
	public int getBanedIs() {
		return banedIs;
	}
	public void setBanedIs(int banedIs) {
		this.banedIs = banedIs;
	}
	public int getWarnedTimes() {
		return warnedTimes;
	}
	public void setWarnedTimes(int warnedTimes) {
		this.warnedTimes = warnedTimes;
	}
	public int getReferUserId() {
		return referUserId;
	}
	public void setReferUserId(int referUserId) {
		this.referUserId = referUserId;
	}
	public Date getUserRegDate() {
		return userRegDate;
	}
	public void setUserRegDate(Date userRegDate) {
		this.userRegDate = userRegDate;
	}
}
