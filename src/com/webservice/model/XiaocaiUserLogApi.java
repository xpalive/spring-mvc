package com.webservice.model;

import java.util.Date;

public class XiaocaiUserLogApi {
	private long id;
	private long userIdP;
	private Date logDate;
	private String logContent;
	private int logTypeId;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getUserIdP() {
		return userIdP;
	}
	public void setUserIdP(long userIdP) {
		this.userIdP = userIdP;
	}
	public Date getLogDate() {
		return logDate;
	}
	public void setLogDate(Date logDate) {
		this.logDate = logDate;
	}
	public String getLogContent() {
		return logContent;
	}
	public void setLogContent(String logContent) {
		this.logContent = logContent;
	}
	public int getLogTypeId() {
		return logTypeId;
	}
	public void setLogTypeId(int logTypeId) {
		this.logTypeId = logTypeId;
	}
}
