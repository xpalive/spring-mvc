package com.webservice;

import java.util.List;

import javax.annotation.Resource;

import com.common.Criteria;
import com.service.UserService;

public class HelloWorldBean implements HelloWorld{
	
	@Resource
	private UserService userService;
	public String greeting(String name) {
		Criteria criteria=new Criteria();
		criteria.setRowCount(10);
        List list= userService.selectByCriteriaPagination(criteria);
        return "你好 "+name;
    }

    public String print() {
        return "我叫林计钦";
    }
}
