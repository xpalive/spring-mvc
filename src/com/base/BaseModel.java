package com.base;

import org.apache.commons.lang.StringUtils;
import com.common.Pager;

import java.util.HashMap;
import java.util.Map;

public abstract class BaseModel {
	private Integer page = 1;
	private Integer rows =10;
	private String sort;
	private String order;
    private Map<String,Object> condition = new HashMap<String,Object>();
	
	/**
	 * 分页导航
	 */
	private Pager pager = new Pager();

	public Pager getPager() {



		return pager;
	}

	public void setPager(Pager pager) {

		this.pager = pager;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
        pager.setPageId(page);
		this.page = page;
	}

	public Integer getRows() {
		return rows;
	}

	public void setRows(Integer rows) {
        pager.setPageSize(rows);
		this.rows = rows;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
        String orderField="id";
        if(StringUtils.isNotBlank(sort)){
            orderField = sort;
        }
        if(StringUtils.isNotBlank(orderField) && StringUtils.isNotBlank(order)){
            orderField +=" "+ order;
        }
        pager.setOrderField(orderField);
		this.order = order;
	}

    public Map<String, Object> getCondition() {
        return condition;
    }

    public void setCondition(Map<String, Object> condition) {
        this.condition = condition;
    }
}
