package com.base;

import com.common.Criteria;

import java.util.List;
import java.util.Map;
/**
 * 数据访问层接口(提供常用方法接口)
 * @author tiangai
 * @param <T>
 */
public interface BaseMapper<T> {
	/**
	 * 插入
	 * @param entity
	 * @return 受影响的行数
	 */
	int insert(T entity);
	/**
	 * 根据主键删除
	 * @param id
	 * @return 受影响的行数
	 */
	int deleteByPrimaryKey(int id);
	/**
	 * 根据条件删除
	 * @param entity
	 * @return 受影响的行数
	 */
	int deleteByCriteria(T entity);
	/**
	 * 批量删除
	 * @param ids
	 * @return 受影响的行数
	 */
	int deleteBatch(int [] ids);
	/**
	 * 根据主键修改
	 * @param entity
	 * @return 受影响的行数
	 */
	int updateByPrimaryKey(T entity);
	/**
	 * 根据条件批量修改
	 * @param map
	 * @return
	 */
	int updateBatch(Map<String, Object> map);
	/**
	 * 根据条件查询(分页)
	 * @param criteria
	 * @return 返回所有查询的结果集
	 */
	List<T> selectByCriteriaPagination(Criteria criteria);
	/**
	 * 根据条件查询(不分页)
	 * @param entity
	 * @return 返回所有查询的结果集
	 */
	List<T> selectByCriteria(T entity);
	/**
	 * 根据主键查询记录
	 */
	T selectByPrimaryKey(long Id);
	/**
	 * 根据条件查出记录总数
	 * @param criteria
	 * @return 记录数
	 */
	int countByCriteria(Criteria criteria);
	
}
