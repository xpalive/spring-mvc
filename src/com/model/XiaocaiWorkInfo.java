package com.model;

public class XiaocaiWorkInfo {
	private long urlId;//所刷的网址
	private long userId;//刷网址的人
	private long point;//要扣除的积分
	private long urlsBelongToUserId;//网址所属人ID
	private int finishedHour;//完成时间点
	public long getUrlId() {
		return urlId;
	}
	public void setUrlId(long urlId) {
		this.urlId = urlId;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public long getPoint() {
		return point;
	}
	public void setPoint(long point) {
		this.point = point;
	}
	public long getUrlsBelongToUserId() {
		return urlsBelongToUserId;
	}
	public void setUrlsBelongToUserId(long urlsBelongToUserId) {
		this.urlsBelongToUserId = urlsBelongToUserId;
	}
	public int getFinishedHour() {
		return finishedHour;
	}
	public void setFinishedHour(int finishedHour) {
		this.finishedHour = finishedHour;
	}
	
}
