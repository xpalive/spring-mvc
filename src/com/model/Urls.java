package com.model;

import java.io.Serializable;

import com.base.BaseModel;

public class Urls extends BaseModel implements Serializable {
	/**
	 * ID
	 */
	private int id;
	/**
	 * 目标网址
	 */
	private String targetUrl;
	/**
	 * 来源网址
	 */
	private String sourceUrl;
	/**
	 * 任务名称
	 */
	private String taskName;
	
	
	
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTargetUrl() {
		return targetUrl;
	}
	public void setTargetUrl(String targetUrl) {
		this.targetUrl = targetUrl;
	}
	public String getSourceUrl() {
		return sourceUrl;
	}
	public void setSourceUrl(String sourceUrl) {
		this.sourceUrl = sourceUrl;
	}
	
	
	
}
