package com.model;

import java.io.Serializable;
import java.util.Date;

import com.base.BaseModel;

public class XiaocaiInput  extends BaseModel implements Serializable
{
    private int id;
    private float inputMoney;//充值金额
    private int inputStatus;//充值状态
    private long userIdP;//充值人ID
    private int adminIdP;//审核人ID
    private Date inputDate;//充值日期
    private Date inputPassDate;//充值审核日期
    private Date inputBak;//充值备注
    private String inputBank;//充值人帐号
    private String inputUserName;//充值人姓名
    private int inputType;//充值还是提现，0为充值，1为提现
    
	public float getInputMoney() {
		return inputMoney;
	}
	public void setInputMoney(float inputMoney) {
		this.inputMoney = inputMoney;
	}
	public int getInputType() {
		return inputType;
	}
	public void setInputType(int inputType) {
		this.inputType = inputType;
	}
	public String getInputBank() {
		return inputBank;
	}
	public void setInputBank(String inputBank) {
		this.inputBank = inputBank;
	}
	public String getInputUserName() {
		return inputUserName;
	}
	public void setInputUserName(String inputUserName) {
		this.inputUserName = inputUserName;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getInputStatus() {
		return inputStatus;
	}
	public void setInputStatus(int inputStatus) {
		this.inputStatus = inputStatus;
	}


	public long getUserIdP() {
		return userIdP;
	}
	public void setUserIdP(long userIdP) {
		this.userIdP = userIdP;
	}
	public int getAdminIdP() {
		return adminIdP;
	}
	public void setAdminIdP(int adminIdP) {
		this.adminIdP = adminIdP;
	}
	public Date getInputDate() {
		return inputDate;
	}
	public void setInputDate(Date inputDate) {
		this.inputDate = inputDate;
	}
	public Date getInputPassDate() {
		return inputPassDate;
	}
	public void setInputPassDate(Date inputPassDate) {
		this.inputPassDate = inputPassDate;
	}
	public Date getInputBak() {
		return inputBak;
	}
	public void setInputBak(Date inputBak) {
		this.inputBak = inputBak;
	}
    
}
