package com.model;

public class XiaocaiWebConfig {
	private String web_name;// 网站名称
	private String web_domain;// 网站域名
	private int level_point_refer_one;// 普通会员刷一个网址获取到的积分百分比
	private int level_point_refer_two;// 铜牌会员刷一个网址获取到的积分百分比
	private int level_point_refer_three;// 银牌会员刷一个网址获取到的积分百分比
	private int level_point_refer_four;// 金牌会员刷一个网址获取到的积分百分比
	
	private int level_refer_agent1;// 普通会员推广下线为代理赚取的百分比
	private int level_refer_agent2;// 铜牌会员推广下线为代理赚取的百分比
	private int level_refer_agent3;// 银牌会员推广下线为代理赚取的百分比
	private int level_refer_agent4;// 金牌会员推广下线为代理赚取的百分比
	
	
	private int point_price_1;// 普通会员花1元可购买10000积分
	private int point_price_2;// 铜牌会员花1元可购买12000积分
	private int point_price_3;// 银牌会员花1元可购买15000积分
	private int point_price_4;// 金牌会员花1元可购买18000积分
	private int min_out_pay_money;// 最小提现金额
	private int min_buy_point;// 最少购买积分数
	private int min_exchange_point;// 最少兑换积分数10000
	private int one_rmb_exchange_point;// 20000积分可以兑换1元rmb
	private int level1_upgrade;// 铜牌升级价格
	private int level2_upgrade;// 银牌升级价格
	private int level3_upgrade;// 金牌升级价格
	private int warn_times_for_ip;// 警告次数（一个IP1分钟内刷多少次警告）
	private int level1_upgrade_gift_point;//铜牌升级赠送的积分数
	private int level2_upgrade_gift_point;//银牌升级赠送的积分数
	private int level3_upgrade_gift_point;//金牌升级赠送的积分数

	public int getLevel_refer_agent1() {
		return level_refer_agent1;
	}

	public void setLevel_refer_agent1(int level_refer_agent1) {
		this.level_refer_agent1 = level_refer_agent1;
	}

	public int getLevel_refer_agent2() {
		return level_refer_agent2;
	}

	public void setLevel_refer_agent2(int level_refer_agent2) {
		this.level_refer_agent2 = level_refer_agent2;
	}

	public int getLevel_refer_agent3() {
		return level_refer_agent3;
	}

	public void setLevel_refer_agent3(int level_refer_agent3) {
		this.level_refer_agent3 = level_refer_agent3;
	}

	public int getLevel_refer_agent4() {
		return level_refer_agent4;
	}

	public void setLevel_refer_agent4(int level_refer_agent4) {
		this.level_refer_agent4 = level_refer_agent4;
	}

	public int getLevel1_upgrade_gift_point() {
		return level1_upgrade_gift_point;
	}

	public void setLevel1_upgrade_gift_point(int level1_upgrade_gift_point) {
		this.level1_upgrade_gift_point = level1_upgrade_gift_point;
	}

	public int getLevel2_upgrade_gift_point() {
		return level2_upgrade_gift_point;
	}

	public void setLevel2_upgrade_gift_point(int level2_upgrade_gift_point) {
		this.level2_upgrade_gift_point = level2_upgrade_gift_point;
	}

	public int getLevel3_upgrade_gift_point() {
		return level3_upgrade_gift_point;
	}

	public void setLevel3_upgrade_gift_point(int level3_upgrade_gift_point) {
		this.level3_upgrade_gift_point = level3_upgrade_gift_point;
	}

	public int getWarn_times_for_ip() {
		return warn_times_for_ip;
	}

	public void setWarn_times_for_ip(int warn_times_for_ip) {
		this.warn_times_for_ip = warn_times_for_ip;
	}

	public String getWeb_name() {
		return web_name;
	}

	public void setWeb_name(String web_name) {
		this.web_name = web_name;
	}

	public String getWeb_domain() {
		return web_domain;
	}

	public void setWeb_domain(String web_domain) {
		this.web_domain = web_domain;
	}

	public int getLevel_point_refer_one() {
		return level_point_refer_one;
	}

	public void setLevel_point_refer_one(int level_point_refer_one) {
		this.level_point_refer_one = level_point_refer_one;
	}

	public int getLevel_point_refer_two() {
		return level_point_refer_two;
	}

	public void setLevel_point_refer_two(int level_point_refer_two) {
		this.level_point_refer_two = level_point_refer_two;
	}

	public int getLevel_point_refer_three() {
		return level_point_refer_three;
	}

	public void setLevel_point_refer_three(int level_point_refer_three) {
		this.level_point_refer_three = level_point_refer_three;
	}

	public int getLevel_point_refer_four() {
		return level_point_refer_four;
	}

	public void setLevel_point_refer_four(int level_point_refer_four) {
		this.level_point_refer_four = level_point_refer_four;
	}

	public int getPoint_price_1() {
		return point_price_1;
	}

	public void setPoint_price_1(int point_price_1) {
		this.point_price_1 = point_price_1;
	}

	public int getPoint_price_2() {
		return point_price_2;
	}

	public void setPoint_price_2(int point_price_2) {
		this.point_price_2 = point_price_2;
	}

	public int getPoint_price_3() {
		return point_price_3;
	}

	public void setPoint_price_3(int point_price_3) {
		this.point_price_3 = point_price_3;
	}

	public int getPoint_price_4() {
		return point_price_4;
	}

	public void setPoint_price_4(int point_price_4) {
		this.point_price_4 = point_price_4;
	}

	public int getMin_out_pay_money() {
		return min_out_pay_money;
	}

	public void setMin_out_pay_money(int min_out_pay_money) {
		this.min_out_pay_money = min_out_pay_money;
	}

	public int getMin_buy_point() {
		return min_buy_point;
	}

	public void setMin_buy_point(int min_buy_point) {
		this.min_buy_point = min_buy_point;
	}

	public int getMin_exchange_point() {
		return min_exchange_point;
	}

	public void setMin_exchange_point(int min_exchange_point) {
		this.min_exchange_point = min_exchange_point;
	}

	public int getOne_rmb_exchange_point() {
		return one_rmb_exchange_point;
	}

	public void setOne_rmb_exchange_point(int one_rmb_exchange_point) {
		this.one_rmb_exchange_point = one_rmb_exchange_point;
	}

	public int getLevel1_upgrade() {
		return level1_upgrade;
	}

	public void setLevel1_upgrade(int level1_upgrade) {
		this.level1_upgrade = level1_upgrade;
	}

	public int getLevel2_upgrade() {
		return level2_upgrade;
	}

	public void setLevel2_upgrade(int level2_upgrade) {
		this.level2_upgrade = level2_upgrade;
	}

	public int getLevel3_upgrade() {
		return level3_upgrade;
	}

	public void setLevel3_upgrade(int level3_upgrade) {
		this.level3_upgrade = level3_upgrade;
	}
}
