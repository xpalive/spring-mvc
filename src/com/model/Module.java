package com.model;

import com.base.BaseModel;

import java.io.Serializable;

public class Module extends BaseModel implements Serializable{
	/**
	 * version
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * id
	 */
	private int id;
	/**
	 * 树节点名称
	 */
	private String name;
	/**
	 * 树图标
	 */
	private String icon;
	/**
	 * 单击树节点访问的路径
	 */
	private String file;
	/**
	 * 父节点
	 */
	private int pId;
	/**
	 * 树节点是不是打开状态
	 */
	private int open;
	/**
	 * 树节点打开时的图标
	 */
	private String iconOpen;
	/**
	 * 树关闭时候的图标
	 */
	private String iconClose;
	/**
	 * tab图标
	 */
	private String tabIcon;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getFile() {
		return file;
	}
	public void setFile(String file) {
		this.file = file;
	}
	public int getpId() {
		return pId;
	}
	public void setpId(int pId) {
		this.pId = pId;
	}
	public int getOpen() {
		return open;
	}
	public void setOpen(int open) {
		this.open = open;
	}
	public String getIconOpen() {
		return iconOpen;
	}
	public void setIconOpen(String iconOpen) {
		this.iconOpen = iconOpen;
	}
	public String getIconClose() {
		return iconClose;
	}
	public void setIconClose(String iconClose) {
		this.iconClose = iconClose;
	}
	public String getTabIcon() {
		return tabIcon;
	}
	public void setTabIcon(String tabIcon) {
		this.tabIcon = tabIcon;
	}
	

}
