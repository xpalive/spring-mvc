package com.model;
public class XiaocaiSources
{
    private int sourceType;
    private String sourceKeyWords;
    private int sourcePercent;
    
    
	public int getSourceType() {
		return sourceType;
	}
	public void setSourceType(int sourceType) {
		this.sourceType = sourceType;
	}
	public String getSourceKeyWords() {
		return sourceKeyWords;
	}
	public void setSourceKeyWords(String sourceKeyWords) {
		this.sourceKeyWords = sourceKeyWords;
	}
	public int getSourcePercent() {
		return sourcePercent;
	}
	public void setSourcePercent(int sourcePercent) {
		this.sourcePercent = sourcePercent;
	}
}
