package com.model;

import java.io.Serializable;

import com.base.BaseModel;

public class XiaocaiAdmin  extends BaseModel implements Serializable{
	private long id;
	private String adminName;
    private String adminPassword;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getAdminName() {
		return adminName;
	}
	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}
	public String getAdminPassword() {
		return adminPassword;
	}
	public void setAdminPassword(String adminPassword) {
		this.adminPassword = adminPassword;
	}
    
}
