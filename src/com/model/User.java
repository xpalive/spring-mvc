package com.model;

import java.io.Serializable;
import java.sql.Date;

import com.base.BaseModel;
/**
 * 实体类
 * @author tiangai
 */
public class User extends BaseModel implements Serializable{
	/**
	 * version
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * ID
	 */
	private int id;
	/**
	 * 账户
	 */
	private String account;
	/**
	 * 密码
	 */
	private String password;
	/**
	 * 真实名字
	 */
	private String real_name;
	/**
	 * 性别
	 */
	private String sex;
	/**
	 * 邮箱
	 */
	private String email;
	/**
	 * 手机
	 */
	private String mobile;
	/**
	 * 地址
	 */
	private String address;
	/**
	 * 出生日期
	 */
	private Date birthday;
	/**
	 * 公司名称
	 */
	private String company;
	/**
	 * 身份证号
	 */
	private String id_number;
	/**
	 * 注册日期
	 */
	private Date register_date;
	/**
	 * 最后登录日期
	 */
	private Date last_login_date;
	/**
	 * 错误次数
	 */
	private int error_count;
	/**
	 * 最后登录IP
	 */
	private String last_login_ip;
	/**
	 * 备注
	 */
	private String remark;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getReal_name() {
		return real_name;
	}
	public void setReal_name(String real_name) {
		this.real_name = real_name;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getId_number() {
		return id_number;
	}
	public void setId_number(String id_number) {
		this.id_number = id_number;
	}
	public Date getRegister_date() {
		return register_date;
	}
	public void setRegister_date(Date register_date) {
		this.register_date = register_date;
	}
	public Date getLast_login_date() {
		return last_login_date;
	}
	public void setLast_login_date(Date last_login_date) {
		this.last_login_date = last_login_date;
	}
	public int getError_count() {
		return error_count;
	}
	public void setError_count(int error_count) {
		this.error_count = error_count;
	}
	public String getLast_login_ip() {
		return last_login_ip;
	}
	public void setLast_login_ip(String last_login_ip) {
		this.last_login_ip = last_login_ip;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	

}
