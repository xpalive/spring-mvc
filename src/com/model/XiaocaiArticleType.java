package com.model;

import java.io.Serializable;

import com.base.BaseModel;

public class XiaocaiArticleType extends BaseModel implements Serializable{
	private int id;
    private String articleTypeName;
    private int articleTypeOrder;
    private int parentId;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getArticleTypeName() {
		return articleTypeName;
	}
	public void setArticleTypeName(String articleTypeName) {
		this.articleTypeName = articleTypeName;
	}
	public int getArticleTypeOrder() {
		return articleTypeOrder;
	}
	public void setArticleTypeOrder(int articleTypeOrder) {
		this.articleTypeOrder = articleTypeOrder;
	}
	public int getParentId() {
		return parentId;
	}
	public void setParentId(int parentId) {
		this.parentId = parentId;
	}
}
