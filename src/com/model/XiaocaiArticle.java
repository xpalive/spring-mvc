package com.model;

import java.io.Serializable;
import java.util.Date;

import com.base.BaseModel;

public class XiaocaiArticle extends BaseModel implements Serializable{
	private int id;
    private String articleTitle;
    private int articleTypeIdP;
    private Date articleCreateDate;
    private int articleClickTimes;
    private String articleContent;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getArticleTitle() {
		return articleTitle;
	}
	public void setArticleTitle(String articleTitle) {
		this.articleTitle = articleTitle;
	}
	public int getArticleTypeIdP() {
		return articleTypeIdP;
	}
	public void setArticleTypeIdP(int articleTypeIdP) {
		this.articleTypeIdP = articleTypeIdP;
	}
	public Date getArticleCreateDate() {
		return articleCreateDate;
	}
	public void setArticleCreateDate(Date articleCreateDate) {
		this.articleCreateDate = articleCreateDate;
	}
	public int getArticleClickTimes() {
		return articleClickTimes;
	}
	public void setArticleClickTimes(int articleClickTimes) {
		this.articleClickTimes = articleClickTimes;
	}
	public String getArticleContent() {
		return articleContent;
	}
	public void setArticleContent(String articleContent) {
		this.articleContent = articleContent;
	}
}
