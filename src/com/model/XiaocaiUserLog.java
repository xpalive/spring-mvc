package com.model;

import java.io.Serializable;
import java.util.Date;

import com.base.BaseModel;

public class XiaocaiUserLog extends BaseModel implements Serializable{
	public static final int USER_UPGRADE=1;//用户升级
	public static final int USER_ACCEPT_BUY_POINT=2;//用户接收积分
	public static final int USER_ACCEPT_SELL_POINT=3;//卖家的积分成功被买家接收
	public static final int USER_TASK_USE_POINT=4;//消费积分
	public static final int DEL_TASK_BACK_POINT=5;//删除任务积分退回
	public static final int USER_INPUT_MONEY=6;//充值申请提交
	public static final int USER_PAY_OUT_MONEY=7;//提现申请提交
	public static final int USER_BUY_POINT=8;//用户购买积分
	public static final int USER_BUY_POINT_USE_MONEY=9;//用户购买积分
	public static final int USER_SELL_POINT=10;//用户购买积分
	public static final int USER_SELL_POINT_GET_MONEY=11;//用户购买积分
	public static final int USER_MOVE_POINT=12;//用户转移积分
	public static final int USER_MOVE_POINT_CANCLE=13;//用户转移积分取消
	
	public static final int ADMIN_INPUT_POINT=14;//管理员充入积分
	public static final int ADMIN_INPUT_MONEY=15;//管理员充入money
	public static final int ADMIN_PAYOUT_PASS=16;//管理员审核提现通过
	public static final int ADMIN_PAYOUT_NO_PASS=17;//管理员审核提现不通过
	public static final int ADMIN_PAYIN_PASS=18;//管理员审核充值通过
	public static final int REFER_AGENT=19;//推广的会员成为代理
	
	
	
	private long id;
	private long userIdP;
	private Date logDate;
	private String logContent;
	private int logTypeId;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getUserIdP() {
		return userIdP;
	}
	public void setUserIdP(long userIdP) {
		this.userIdP = userIdP;
	}
	public Date getLogDate() {
		return logDate;
	}
	public void setLogDate(Date logDate) {
		this.logDate = logDate;
	}
	public String getLogContent() {
		return logContent;
	}
	public void setLogContent(String logContent) {
		this.logContent = logContent;
	}
	public int getLogTypeId() {
		return logTypeId;
	}
	public void setLogTypeId(int logTypeId) {
		this.logTypeId = logTypeId;
	}
	
}
