package com.service;

import java.util.List;

import com.common.Criteria;
import com.model.XiaocaiInput;

public interface XiaocaiInputService {
	List<XiaocaiInput> selectByCriteria(XiaocaiInput xiaocaiInput);
    List<XiaocaiInput> selectByCriteriaPagination(Criteria criteria);
    XiaocaiInput selectByPrimaryKey(int Id);
    int addxiaocaiInputService(XiaocaiInput xiaocaiInput);
    int updateXiaocaiInputService(XiaocaiInput xiaocaiInput);
    int getTotal(Criteria criteria);
    void deleteBatch(int [] ids);
}
