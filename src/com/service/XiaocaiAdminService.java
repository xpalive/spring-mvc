package com.service;

import java.util.List;

import com.common.Criteria;
import com.model.XiaocaiAdmin;

public interface XiaocaiAdminService {
	List<XiaocaiAdmin> selectByCriteria(XiaocaiAdmin xiaocaiAdmin);
    List<XiaocaiAdmin> selectByCriteriaPagination(Criteria criteria);
    XiaocaiAdmin login(XiaocaiAdmin xiaocaiAdmin);
    int getTotal(Criteria criteria);
    void deleteBatch(int [] ids);

}
