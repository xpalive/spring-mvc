package com.service;

import com.model.XiaocaiWebConfig;

public interface WebConfigInfoService {
	public String getWebName();
	public boolean changeWebName(String webName);
	public XiaocaiWebConfig getXiaocaiWebConfig();
	public boolean setXiaocaiWebConfig(XiaocaiWebConfig xiaocaiWebConfig);
}
