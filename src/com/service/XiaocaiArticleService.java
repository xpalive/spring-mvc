package com.service;

import java.util.List;

import com.common.Criteria;
import com.model.XiaocaiArticle;

public interface XiaocaiArticleService {
	List<XiaocaiArticle> selectByCriteria(XiaocaiArticle xiaocaiArticle);
    List<XiaocaiArticle> selectByCriteriaPagination(Criteria criteria);
    XiaocaiArticle selectByPrimaryKey(int Id);
    int addXiaocaiArticle(XiaocaiArticle xiaocaiArticle);
    int editXiaocaiArticle(XiaocaiArticle xiaocaiArticle);
    int getTotal(Criteria criteria);
    void deleteBatch(int [] ids);
}
