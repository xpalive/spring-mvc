package com.service;

import java.util.List;

import com.common.Criteria;
import com.model.XiaocaiUserLog;

public interface XiaocaiUserLogService {
	List<XiaocaiUserLog> selectByCriteria(XiaocaiUserLog xiaocaiUserLog);
    List<XiaocaiUserLog> selectByCriteriaPagination(Criteria criteria);
    XiaocaiUserLog selectByPrimaryKey(int Id);
    int addXiaocaiUserLogService(XiaocaiUserLog xiaocaiUserLog);
    int addXiaocaiUserLogService(long userIdP,int logTypeId,String logContent);
    //int updateXiaocaiUserLogService(XiaocaiUserLog xiaocaiUserLog);
    int getTotal(Criteria criteria);
    void deleteBatch(int [] ids);
}
