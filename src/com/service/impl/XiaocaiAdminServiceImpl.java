package com.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.common.Criteria;
import com.dao.XiaocaiAdminMapper;
import com.model.XiaocaiAdmin;
import com.service.XiaocaiAdminService;
@Service
public class XiaocaiAdminServiceImpl implements XiaocaiAdminService{

	@Resource
	private XiaocaiAdminMapper xiaocaiAdminMapper;
	@Override
	public List<XiaocaiAdmin> selectByCriteria(XiaocaiAdmin xiaocaiAdmin) {
		// TODO Auto-generated method stub
		return xiaocaiAdminMapper.selectByCriteria(xiaocaiAdmin);
	}

	@Override
	public List<XiaocaiAdmin> selectByCriteriaPagination(Criteria criteria) {
		// TODO Auto-generated method stub
		return xiaocaiAdminMapper.selectByCriteriaPagination(criteria);
	}

	@Override
	public XiaocaiAdmin login(XiaocaiAdmin xiaocaiAdmin) {
		// TODO Auto-generated method stub
		List<XiaocaiAdmin> list= xiaocaiAdminMapper.selectByCriteria(xiaocaiAdmin);
		if(null!=list&&list.size()>0)
        {
			xiaocaiAdmin=list.get(0);
        }else
        {
        	xiaocaiAdmin=null;
        }
        return xiaocaiAdmin;
	}

	@Override
	public int getTotal(Criteria criteria) {
		// TODO Auto-generated method stub
		return xiaocaiAdminMapper.countByCriteria(criteria);
	}

	@Override
	public void deleteBatch(int[] ids) {
		xiaocaiAdminMapper.deleteBatch(ids);
	}

}
