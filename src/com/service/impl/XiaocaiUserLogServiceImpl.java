package com.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.common.Criteria;
import com.dao.XiaocaiUserLogMapper;
import com.model.XiaocaiUserLog;
import com.service.XiaocaiUserLogService;
@Service
public class XiaocaiUserLogServiceImpl implements XiaocaiUserLogService{

	@Resource
	private XiaocaiUserLogMapper xiaocaiUserLogMapper;
	@Override
	public List<XiaocaiUserLog> selectByCriteria(XiaocaiUserLog xiaocaiUserLog) {
		// TODO Auto-generated method stub
		return xiaocaiUserLogMapper.selectByCriteria(xiaocaiUserLog);
	}

	@Override
	public List<XiaocaiUserLog> selectByCriteriaPagination(Criteria criteria) {
		// TODO Auto-generated method stub
		return xiaocaiUserLogMapper.selectByCriteriaPagination(criteria);
	}

	@Override
	public XiaocaiUserLog selectByPrimaryKey(int Id) {
		// TODO Auto-generated method stub
		return xiaocaiUserLogMapper.selectByPrimaryKey(Id);
	}

	@Override
	public int addXiaocaiUserLogService(XiaocaiUserLog xiaocaiUserLog) {
		// TODO Auto-generated method stub
		return xiaocaiUserLogMapper.insert(xiaocaiUserLog);
	}

	public int addXiaocaiUserLogService(long userIdP,int logTypeId,String logContent) {
		XiaocaiUserLog xiaocaiUserLog=new XiaocaiUserLog();
		xiaocaiUserLog.setUserIdP(userIdP);
		xiaocaiUserLog.setLogContent(logContent);
		xiaocaiUserLog.setLogDate(new Date());
		xiaocaiUserLog.setLogTypeId(logTypeId);
		return xiaocaiUserLogMapper.insert(xiaocaiUserLog);
	}

	
	@Override
	public int getTotal(Criteria criteria) {
		// TODO Auto-generated method stub
		return xiaocaiUserLogMapper.countByCriteria(criteria);
	}

	@Override
	public void deleteBatch(int[] ids) {
		// TODO Auto-generated method stub
		xiaocaiUserLogMapper.deleteBatch(ids);
	}

}
