package com.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.common.Criteria;
import com.dao.XiaocaiUserMapper;
import com.google.gson.Gson;
import com.model.XiaocaiUser;
import com.service.XiaocaiUserService;

@Service
public class XiaocaiUserServiceImpl implements XiaocaiUserService {

	@Resource
	private XiaocaiUserMapper xiaocaiUserMapper;
	@Resource
	private RedisBase redisBase;
	
	@Override
	public List<XiaocaiUser> selectByCriteria(XiaocaiUser xiaocaiUser) {
		// TODO Auto-generated method stub
		return xiaocaiUserMapper.selectByCriteria(xiaocaiUser);
	}

	@Override
	public List<XiaocaiUser> selectByCriteriaPagination(Criteria criteria) {
		// TODO Auto-generated method stub
		return xiaocaiUserMapper.selectByCriteriaPagination(criteria);
	}

	@Override
	public XiaocaiUser login(XiaocaiUser xiaocaiUser) {
		// TODO Auto-generated method stub
		List<XiaocaiUser> listXiaocaiUsers = xiaocaiUserMapper
				.selectByCriteria(xiaocaiUser);
		if ((null != listXiaocaiUsers) && listXiaocaiUsers.size() > 0)
			return listXiaocaiUsers.get(0);
		else {
			return null;
		}
	}

	@Override
	public int getTotal(Criteria criteria) {
		// TODO Auto-generated method stub
		return xiaocaiUserMapper.countByCriteria(criteria);
	}

	@Override
	public void deleteBatch(int[] ids) {
		// TODO Auto-generated method stub
		xiaocaiUserMapper.deleteBatch(ids);
	}

	@Override
	public boolean isExistByXiaocaiUser(XiaocaiUser xiaocaiUser) {
		List<XiaocaiUser> listXiaocaiUsers = xiaocaiUserMapper
				.selectByCriteria(xiaocaiUser);
		if (null != listXiaocaiUsers && (listXiaocaiUsers.size() > 0)) {
			return true;
		}
		return false;
	}

	/**
	 * 用户注册
	 */
	@Override
	public XiaocaiUser add(XiaocaiUser xiaocaiUser) {
		int col = xiaocaiUserMapper.insert(xiaocaiUser);
		if (col > 0) {
			return xiaocaiUser;
		} else {
			return null;
		}
	}

	@Override
	public XiaocaiUser selectByPrimaryKey(long id) {
		// TODO Auto-generated method stub
		return xiaocaiUserMapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKey(XiaocaiUser xiaocaiUser) {
		// TODO Auto-generated method stub
		return xiaocaiUserMapper.updateByPrimaryKey(xiaocaiUser);
	}

	@Override
	public void addPoint(long userId, float point) {
		XiaocaiUser xiaocaiUser=new XiaocaiUser();
		xiaocaiUser.setId(userId);
		xiaocaiUser.setUserPoint(point);
		xiaocaiUserMapper.addPoint(xiaocaiUser);
	}
	public void addMoney(long userId, float userMoney) {
		XiaocaiUser xiaocaiUser=new XiaocaiUser();
		xiaocaiUser.setId(userId);
		xiaocaiUser.setUserMoney(userMoney);
		xiaocaiUserMapper.addMoney(xiaocaiUser);
	}
	@Override
	public void addWarned(long userId, int warnedTimes) {
		// TODO Auto-generated method stub
		XiaocaiUser xiaocaiUser=new XiaocaiUser();
		xiaocaiUser.setId(userId);
		xiaocaiUser.setWarnedTimes(warnedTimes);
		xiaocaiUserMapper.addWarned(xiaocaiUser);
	}
	/**
	 * 根据用户帐号获取用户信息
	 */
	@Override
	public XiaocaiUser selectByXiaocaiUserName(String xiaocaiUserName) {
		XiaocaiUser xiaocaiUser=new XiaocaiUser();
		xiaocaiUser.setUserEmail(xiaocaiUserName);
		List<XiaocaiUser> listXiaocaiUser=xiaocaiUserMapper.selectByCriteria(xiaocaiUser);
		if (null!=listXiaocaiUser&&(listXiaocaiUser.size()>0)) {
			return listXiaocaiUser.get(0);
		}
		return null;
	}

	@Override
	public XiaocaiUser selectByPrimaryKeyByRedis(long id) {
		//从redis中查找是否存在此会员
		Gson gson = new Gson();
		XiaocaiUser xiaocaiUser;
		if(redisBase.HASH.hexists("user_temp", String.valueOf(id))){
			 xiaocaiUser=gson.fromJson(redisBase.HASH.hget("user_temp", String.valueOf(id)),XiaocaiUser.class);
			return xiaocaiUser;
		}else {
			 xiaocaiUser=selectByPrimaryKey(id);
			redisBase.HASH.hset("user_temp", String.valueOf(id),gson.toJson(xiaocaiUser));
		}
		return xiaocaiUser;
	}
}
