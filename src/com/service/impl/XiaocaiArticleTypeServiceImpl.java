package com.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.common.Criteria;
import com.dao.XiaocaiArticleTypeMapper;
import com.model.XiaocaiArticleType;
import com.service.XiaocaiArticleTypeService;
@Service
public class XiaocaiArticleTypeServiceImpl implements XiaocaiArticleTypeService{

	@Resource
	private XiaocaiArticleTypeMapper  xiaocaiArticleTypeMapper ;
	@Override
	public List<XiaocaiArticleType> selectByCriteria(
			XiaocaiArticleType xiaocaiArticleType) {
		// TODO Auto-generated method stub
		return xiaocaiArticleTypeMapper.selectByCriteria(xiaocaiArticleType);
	}

	@Override
	public List<XiaocaiArticleType> selectByCriteriaPagination(Criteria criteria) {
		// TODO Auto-generated method stub
		return xiaocaiArticleTypeMapper.selectByCriteriaPagination(criteria);
	}

	@Override
	public XiaocaiArticleType selectByPrimaryKey(int Id) {
		// TODO Auto-generated method stub
		return xiaocaiArticleTypeMapper.selectByPrimaryKey(Id);
	}

	@Override
	public int addxiaocaiInputService(XiaocaiArticleType xiaocaiArticleType) {
		// TODO Auto-generated method stub
		return xiaocaiArticleTypeMapper.insert(xiaocaiArticleType);
	}

	@Override
	public int getTotal(Criteria criteria) {
		// TODO Auto-generated method stub
		return xiaocaiArticleTypeMapper.countByCriteria(criteria);
	}

	@Override
	public void deleteBatch(int[] ids) {
		// TODO Auto-generated method stub
		xiaocaiArticleTypeMapper.deleteBatch(ids);
	}

	@Override
	public int editxiaocaiInputService(XiaocaiArticleType xiaocaiArticleType) {
		// TODO Auto-generated method stub
		return xiaocaiArticleTypeMapper.updateByPrimaryKey(xiaocaiArticleType);
	}
	
}
