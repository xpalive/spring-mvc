package com.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.common.Criteria;
import com.dao.XiaocaiInputMapper;
import com.dao.XiaocaiUserMapper;
import com.model.XiaocaiInput;
import com.model.XiaocaiUser;
import com.service.XiaocaiInputService;

@Service
public class XiaocaiInputServiceImpl implements XiaocaiInputService{

	@Resource
	private XiaocaiInputMapper xiaocaiInputMapper;
	@Resource
	private XiaocaiUserMapper xiaocaiUserMapper;
	
	@Override
	public List<XiaocaiInput> selectByCriteria(XiaocaiInput xiaocaiInput) {
		// TODO Auto-generated method stub
		return xiaocaiInputMapper.selectByCriteria(xiaocaiInput);
	}

	@Override
	public List<XiaocaiInput> selectByCriteriaPagination(Criteria criteria) {
		// TODO Auto-generated method stub
		return xiaocaiInputMapper.selectByCriteriaPagination(criteria);
	}

	@Override
	public XiaocaiInput selectByPrimaryKey(int Id) {
		return xiaocaiInputMapper.selectByPrimaryKey(Id);
	}

	@Override
	public int getTotal(Criteria criteria) {
		// TODO Auto-generated method stub
		return xiaocaiInputMapper.countByCriteria(criteria);
	}

	@Override
	public void deleteBatch(int[] ids) {
		// TODO Auto-generated method stub
		xiaocaiInputMapper.deleteBatch(ids);
	}

	@Override
	public int addxiaocaiInputService(XiaocaiInput xiaocaiInput) {
		// TODO Auto-generated method stub
		int result= xiaocaiInputMapper.insert(xiaocaiInput);
		//提现操作
		if(xiaocaiInput.getInputType()==1){
			//用户可用金额减少，用户冻结资金增加
			XiaocaiUser entity=xiaocaiUserMapper.selectByPrimaryKey(xiaocaiInput.getUserIdP());
			entity.setUserMoney((float) (entity.getUserMoney()-xiaocaiInput.getInputMoney()));
			entity.setFrozenMoney((float) (entity.getFrozenMoney()+xiaocaiInput.getInputMoney()));
			xiaocaiUserMapper.updateByPrimaryKey(entity);
		}
		return result;
	}

	@Override
	public int updateXiaocaiInputService(XiaocaiInput xiaocaiInput) {
		return xiaocaiInputMapper.updateByPrimaryKey(xiaocaiInput);
	}

}
