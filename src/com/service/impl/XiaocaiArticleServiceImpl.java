package com.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.common.Criteria;
import com.dao.XiaocaiArticleMapper;
import com.model.XiaocaiArticle;
import com.service.XiaocaiArticleService;
@Service
public class XiaocaiArticleServiceImpl implements XiaocaiArticleService{

	@Resource
	private XiaocaiArticleMapper xiaocaiArticleMapper;
	
	@Override
	public List<XiaocaiArticle> selectByCriteria(XiaocaiArticle xiaocaiArticle) {
		// TODO Auto-generated method stub
		return xiaocaiArticleMapper.selectByCriteria(xiaocaiArticle);
	}

	@Override
	public List<XiaocaiArticle> selectByCriteriaPagination(Criteria criteria) {
		// TODO Auto-generated method stub
		return xiaocaiArticleMapper.selectByCriteriaPagination(criteria);
	}

	@Override
	public XiaocaiArticle selectByPrimaryKey(int Id) {
		// TODO Auto-generated method stub
		return xiaocaiArticleMapper.selectByPrimaryKey(Id);
	}

	@Override
	public int addXiaocaiArticle(XiaocaiArticle xiaocaiArticle) {
		// TODO Auto-generated method stub
		return xiaocaiArticleMapper.insert(xiaocaiArticle);
	}

	@Override
	public int editXiaocaiArticle(XiaocaiArticle xiaocaiArticle) {
		// TODO Auto-generated method stub
		return xiaocaiArticleMapper.updateByPrimaryKey(xiaocaiArticle);
	}

	@Override
	public int getTotal(Criteria criteria) {
		// TODO Auto-generated method stub
		return xiaocaiArticleMapper.countByCriteria(criteria);
	}

	@Override
	public void deleteBatch(int[] ids) {
		// TODO Auto-generated method stub
		xiaocaiArticleMapper.deleteBatch(ids);
	}

}
