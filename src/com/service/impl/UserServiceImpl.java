package com.service.impl;

import java.util.List;
import javax.annotation.Resource;

import com.common.Criteria;
import org.springframework.stereotype.Service;
import com.dao.UserMapper;
import com.model.User;
import com.service.UserService;

@Service
public class UserServiceImpl implements UserService{

    @Resource
    private UserMapper userMapper;

    private static int totalCount;
    
    public int getTotalCount() {
		return totalCount;
	}
	public List<User> selectByCriteria(User user) {
        // TODO Auto-generated method stub
        return userMapper.selectByCriteria(user);
    }

    @Override
    public List<User> selectByCriteriaPagination(Criteria criteria) {
        return userMapper.selectByCriteriaPagination(criteria);
    }

    public User login(User user) {
        // TODO Auto-generated method stub
        List<User> list=userMapper.selectByCriteria(user);
        if(null!=list&&list.size()>0)
        {
            user=list.get(0);
        }else
        {
            user=null;
        }

        return user;
    }

    public int getTotal(Criteria criteria) {
    	if(totalCount==0){
    		totalCount= userMapper.countByCriteria(criteria);
    		return totalCount;
    	}
    	else
    		return totalCount;
    }
    public void deleteBatch(int[] ids) {
        userMapper.deleteBatch(ids);
    }


}
