package com.service;

import java.util.List;
import com.model.Module;

public interface ModuleService {
	List<Module> selectByCriteria(Module module);

}
