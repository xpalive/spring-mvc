package com.service;

import java.util.List;

import com.common.Criteria;
import com.model.Urls;

public interface UrlsService {
	List<Urls> selectByCriteria(Urls urls);
    List<Urls> selectByCriteriaPagination(Criteria criteria);
    int getTotal(Criteria criteria);
    void deleteBatch(int [] ids);
}
