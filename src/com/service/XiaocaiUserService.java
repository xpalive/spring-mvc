package com.service;

import java.util.List;

import com.common.Criteria;
import com.model.XiaocaiUser;

public interface XiaocaiUserService {
	List<XiaocaiUser> selectByCriteria(XiaocaiUser xiaocaiUser);
	XiaocaiUser selectByXiaocaiUserName(String xiaocaiUserName);
	
	int updateByPrimaryKey(XiaocaiUser xiaocaiUser);
	boolean isExistByXiaocaiUser(XiaocaiUser xiaocaiUser);
    List<XiaocaiUser> selectByCriteriaPagination(Criteria criteria);
    XiaocaiUser login(XiaocaiUser xiaocaiUser);
    int getTotal(Criteria criteria);
    XiaocaiUser selectByPrimaryKey(long id);
    XiaocaiUser selectByPrimaryKeyByRedis(long id);
    void deleteBatch(int [] ids);
    public XiaocaiUser add(XiaocaiUser xiaocaiUser);
	void addPoint(long userId, float point);
	public void addMoney(long userId, float userMoney);
	void addWarned(long userId, int warnedTimes);
}
