package com.common;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.util.TypeCaseHelper;

/**
 * 公用条件查询类<br>
 * 也可以用于MVC层之间的参数传递
 */
public class Criteria {
    private Integer page = 1;             // 当前页
    private Integer rows =10;             // 页大小
    private String sort = "desc";         // 排序方式
    private String order = "id";          // 排序字段
    private Integer rowCount;             // 总行数
    private Integer pageCount;            // 总页数
    private Integer pageStart;            // 总页数
    private Integer pageSize=10;             // 页大小
	private Map<String, Object> condition;//查询条件
	/**
     * 绑定的内容列表
     */
    private List<?> elements;
	
    public List<?> getElements() {
		return elements;
	}


	public void setElements(List<?> elements) {
		this.elements = elements;
	}


	/**
     * <默认构造函数>
     */
    public Criteria(int totalCount, int pageSize, int currentPage)
    {
        setRowCount(totalCount);
        setPageSize(pageSize);
        setPage(currentPage);
    }
	
	
	public Criteria() {
		condition = new HashMap<String, Object>();
	}

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public Integer getRowCount() {
        return rowCount;
    }

    public void setRowCount(Integer rowCount) {
        this.rowCount = rowCount;
        this.doPage();
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public void setPageCount(Integer pageCount) {
        this.pageCount = pageCount;
    }

    public Integer getPageStart() {
        return pageStart;
    }

    public void setPageStart(Integer pageStart) {
        this.pageStart = pageStart;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public void setCondition(Map<String, Object> condition) {
        this.condition = condition;
    }

    public Map<String, Object> getCondition() {
        return condition;
    }

    public void clear() {
        this.condition.clear();
    }

    /**
     * 初始化分页的参数值
     */
    protected void doPage() {
        this.pageSize = this.getRows();
        this.pageCount = this.rowCount / this.pageSize + 1;
        if ((this.rowCount % this.pageSize == 0) && pageCount > 1)
        {
            this.pageCount--;
        }
        // Mysql 算法
        if(page>pageCount)
        {
            page=1;
        }
        this.pageStart = (this.page - 1) * this.pageSize;

    }
    /**
     * @param condition
     *  查询的条件名称
     * @param value
     *  查询的值
     */
    public Criteria put(String condition, Object value) {
        this.condition.put(condition, value);
        return  this;
    }
    /**
     * 得到键值，C层和S层的参数传递时取值所用<br>
     * 自行转换对象
     *
     * @param key
     *            键值
     * @return 返回指定键所映射的值
     */
    public Object get(String key) {
        return this.condition.get(key);
    }
	/**
	 * 以BigDecimal类型返回键值
	 * 
	 * @param key
	 *            键名
	 * @return BigDecimal 键值
	 */
	public BigDecimal getAsBigDecimal(String key) {
		Object obj = TypeCaseHelper.convert(get(key), "BigDecimal", null);
		if (obj != null)
			return (BigDecimal) obj;
		else
			return null;
	}

	/**
	 * 以Date类型返回键值
	 * 
	 * @param key
	 *            键名
	 * @return Date 键值
	 */
	public Date getAsDate(String key) {
		Object obj = TypeCaseHelper.convert(get(key), "Date", "yyyy-MM-dd");
		if (obj != null)
			return (Date) obj;
		else
			return null;
	}

	/**
	 * 以Integer类型返回键值
	 * 
	 * @param key
	 *            键名
	 * @return Integer 键值
	 */
	public Integer getAsInteger(String key) {
		Object obj = TypeCaseHelper.convert(get(key), "Integer", null);
		if (obj != null)
			return (Integer) obj;
		else
			return null;
	}

	/**
	 * 以Long类型返回键值
	 * 
	 * @param key
	 *            键名
	 * @return Long 键值
	 */
	public Long getAsLong(String key) {
		Object obj = TypeCaseHelper.convert(get(key), "Long", null);
		if (obj != null)
			return (Long) obj;
		else
			return null;
	}

	/**
	 * 以String类型返回键值
	 * 
	 * @param key
	 *            键名
	 * @return String 键值
	 */
	public String getAsString(String key) {
		Object obj = TypeCaseHelper.convert(get(key), "String", null);
		if (obj != null)
			return (String) obj;
		else
			return "";
	}

	/**
	 * 以Timestamp类型返回键值
	 * 
	 * @param key
	 *            键名
	 * @return Timestamp 键值
	 */
	public Timestamp getAsTimestamp(String key) {
		Object obj = TypeCaseHelper.convert(get(key), "Timestamp", "yyyy-MM-dd HH:mm:ss");
		if (obj != null)
			return (Timestamp) obj;
		else
			return null;
	}

	/**
	 * 以Boolean类型返回键值
	 * 
	 * @param key
	 *            键名
	 * @return Timestamp 键值
	 */
	public Boolean getAsBoolean(String key) {
		Object obj = TypeCaseHelper.convert(get(key), "Boolean", null);
		if (obj != null)
			return (Boolean) obj;
		else
			return null;
	}

}