package com.common;

import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;

/**
 * Ext Grid返回对象
 * 
 */
public class GridReturn {

	/**
	 * 总共条数
	 */
	private int total;
	/**
	 * 所有数据
	 */
	private List<?> rows;
	/**
	 * 标记
	 */
	private String flag;

	public GridReturn() {
	}

	public GridReturn(int total, List<?> rows) {
		this.total = total;
		this.rows = rows;
	}

	public GridReturn(int total, List<?> rows, String flag) {
		super();
		this.total = total;
		this.rows = rows;
		this.flag = flag;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {

        this.total = total;
	}

	public List<?> getRows() {
		return rows;
	}

	public void setRows(List<?> rows) {
		this.rows = rows;
	}
	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String toString() {

		return JSON.toJSONString(this, SerializerFeature.WriteDateUseDateFormat, SerializerFeature.PrettyFormat);
	}




}
