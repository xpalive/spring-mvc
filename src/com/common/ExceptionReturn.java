package com.common;

import java.io.PrintWriter;
import java.io.StringWriter;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;

/**
 * Ext 异常返回对象
 * 
 */
public class ExceptionReturn {

	public ExceptionReturn() {
	}

	/**
	 * 异常时的构造方法
	 * 
	 * @param msg
	 *            异常消息
	 */
	public ExceptionReturn(Throwable exceptionMessage) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		exceptionMessage.printStackTrace(pw);
		// 异常情况
		this.success = false;
		//太详细了
		// this.exceptionMessage = sw.toString();
		this.exceptionMessage = exceptionMessage.getMessage();
	}

	/**
	 * 是否成功
	 */
	private boolean success;
	/**
	 * 异常的消息
	 */
	private Object exceptionMessage;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public Object getExceptionMessage() {
		return exceptionMessage;
	}

	public void setExceptionMessage(Object exceptionMessage) {
		this.exceptionMessage = exceptionMessage;
	}
	public String toString() {

		return JSON.toJSONString(this, SerializerFeature.WriteDateUseDateFormat, SerializerFeature.PrettyFormat);
	}
}
