package com.common;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.base.BaseControllers;
import com.util.WebConstants;

public class AdminCommonInterceptor  extends BaseControllers implements HandlerInterceptor {

	@Override
	public void afterCompletion(HttpServletRequest arg0,
			HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1,
			Object arg2, ModelAndView arg3) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
			Object arg2) throws Exception {
		Object adminSession=request.getSession().getAttribute(WebConstants.CURRENT_ADMIN);
		String url=request.getRequestURL().toString();
		//System.out.println(url);
		if(null==adminSession){
			if(url.indexOf(".json")>0)
				writerJson(response, errorMsg("登录超时，请重新登录！"));
			else
				response.sendRedirect("/adminlogin");
		}
		return true;
	}

}
