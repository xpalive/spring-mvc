<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html>
<head>
<%@ include file="/resources/common/jsp/headlibs.jsp" %>
<link rel="stylesheet" type="text/css" href="${ctx}/resources/common/css/user_reg.css">
<link rel="stylesheet" type="text/css" href="${ctx}/resources/common/css/user_base.css">
<script>
$(document).ready(function() {
	$.formValidator.initConfig({formID:"regFrom",onError:function(msg){$.jBox.tip(msg, 'error')}});
	$("#userEmail").formValidator({ onShow: "请输入你的email", onfocus: "请注意你输入的email格式，例如:wzmaodong@126.com", onCorrect: "谢谢你的合作，你的email正确" }).regexValidator({ regExp: "email", dataType: "enum", onError: "email格式不正确" }).ajaxValidator({
		dataType : "json",
		async : true,
		url : "/valiadteReg.json?validateCode=1",
		success : function(data){
            if(data.success) {
            	return true;
            }
			return false;
		},
		error: function(jqXHR, textStatus, errorThrown){$.jBox.tip("服务器没有返回数据，可能服务器忙，请重试"+errorThrown);},
		onError : "此邮箱已经注册",
		onWait : "正在对邮箱进行合法性校验，请稍候..."
	});
	$("#userPassword").formValidator({onShow:"请输入密码",onFocus:"6-32位数字或字母",onCorrect:"输入正确"}).inputValidator({min:6,max:32,empty:{leftEmpty:false,rightEmpty:false,emptyError:"密码两边不能有空符号"},onError:"密码输入位数不正确，请检查"});
	$("#repass").formValidator({onShow:"再次输入密码",onFocus:"输入应与密码一致",onCorrect:"输入正确"}).inputValidator({min:6,max:32,empty:{leftEmpty:false,rightEmpty:false,emptyError:"确认密码两边不能有空符号"},onError:"密码输入位数不正确，请检查"}).compareValidator({desID:"userPassword",operateor:"=",onError:"2次密码输入不一致"});
	$("#userQQ").formValidator({ onShow: "请输入QQ号码", onCorrect: "谢谢你的合作，你的QQ号码正确" }).regexValidator({ regExp: "qq", dataType: "enum", onError: "QQ号码格式不正确" }); 
	$("#authCodeReg").formValidator({onShow:"请输入验证码",onFocus:"4位验证码",onCorrect:"输入正确"}).inputValidator({min:4,max:4,empty:{leftEmpty:false,rightEmpty:false,emptyError:"验证码两边不能有空符号"},onError:"验证码输入位数不正确，请检查"});
	
});
function registFormCommit() {
	if(!jQuery.formValidator.pageIsValid()) return false;
	$.jBox.tip('系统正在处理中，请稍后!','loading');
	var params = $("#regFrom").serialize();
	$.post('/reg.json',
		params,
		callbackRegister,
		'json'
	).error(function() { $.jBox.tip('服务器异常','error'); });
}
function callbackRegister(objResult) {
	if(objResult.success == false) {
		$.jBox.tip(objResult.msg,'error');
	}else{
		$.jBox.tip('用户注册成功！','success', {closed:function(){location.href="/user";}});
	}
}
function refresh(){
	setTimeout("_refresh()", 700);
}
function _refresh() {
	var src = document.getElementById("authImg").src;
	document.getElementById("authImg").src = src + "?now="
			+ new Date().getTime();
}
</script>
<title>星云流量互换系统-注册</title>
</head>
<body>
<%@ include file="/resources/common/jsp/head.jsp" %>
<div class="w980 mt10 mc">
<form id="regFrom" name="regFrom">
        <div class="w900 bt">
           <div class="title">会员注册</div>
           <div id="userbase">
               <div class="left">
                   <ul id="reg">
                       <li><span>电子邮箱：</span><input type="text" name="userEmail" id="userEmail" class="ip w01 c_username" maxlength="20"  /><em class="tip" id="userEmailTip">请输入邮箱</em></li>
                       <li><span>密码：</span><input type="password" name="userPassword" id="userPassword" class="ip w01 c_password" maxlength="16"  /><em class="tip" id="userPasswordTip">由6-16个数字、字母、下划线组成</em></li>
                       <li><span>再次输入密码：</span><input type="password" name="repass" id="repass" class="ip w01 c_repass" maxlength="16" /><em class="tip" id="repassTip">请再次输入密码</em></li>
                       <li><span>联系QQ：</span><input type="text" name="userQQ" id="userQQ" class="ip w01 c_repass" maxlength="16"  /><em class="tip" id="userQQTip">请输入QQ</em></li>
                       <li><span>推荐人ID：</span><input type="text" name="referUserId" id="referUserId" class="ip w01 c_repass" value="${referUserId}"  readOnly/></li>
                       <li><span>验证码：</span><input type="text" name="authCodeReg" id="authCodeReg" class="ip w03 c_code" maxlength="4" required="true" /> 
                       <img onclick="refresh();" id="authImg" src="/authimg" align="absmiddle" /><em class="tip" id="authCodeRegTip">请输入左侧图片上的文字</em></li>
                  		<dd><img src="/resources/common/images/bnt_reg.gif" onClick="registFormCommit();" /></dd>
                   </ul>
                   
               </div>
               <div class="midline"></div>
               <div class="info">
                   <p>已有帐号？</p>
                   <p class="bnt"><a href="/login">立即登录</a></p>
                   <p>想快速体验？使用以下帐号登录： </p>
                   
               </div>
           </div>
        </div>
  </form>
</div>
<%@ include file="/resources/common/jsp/foot.jsp" %>
</body>
</html>
