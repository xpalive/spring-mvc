﻿<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ include file="/resources/common/jsp/taglibs.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>TNT(服务器监控系统)</title>
    <link rel="stylesheet" type="text/css" href="${ctx}/resources/jquery-zTree-3.5.15/css/zTreeStyle/zTreeStyle.css">
    <script type="text/javascript" src="${ctx}/resources/jquery-zTree-3.5.15/jquery.ztree.all-3.5.min.js"></script>
    <script type="text/javascript" src="${ctx}/resources/js/main.js"></script>
</head>

<body class="easyui-layout">
<!-- 头部标题 -->
<div data-options="region:'north',border:false" style="height:60px; padding:5px; background:#F3F3F3">
    <a href="index.jsp"><span class="northTitle">TNT(服务器监控系统)</span></a>
    <span class="loginInfo">登录用户：${user.account}&nbsp;&nbsp;姓名：${user.real_name}&nbsp;&nbsp;</span>
</div>

<!-- 左侧导航 -->
<div data-options="region:'west',split:true,title:'导航菜单', fit:false,tools:'#tree_top_btn'" style="width:200px;">
    <ul id="menuTree" class="ztree">
    </ul>
</div>
<div id="tree_top_btn">
    <a href="javascript:void(0)"  id="save" class="easyui-reload" iconCls="icon-add" plain="true"></a>
</div>
<!-- 页脚信息 -->
<div data-options="region:'south',border:false"
     style="height:20px; background:#F3F3F3; padding:2px; vertical-align:middle;">
    <span id="sysVersion">系统版本：V1.0</span>
    <span id="nowTime"></span>
</div>

<!-- 内容tabs -->
<div id="center" data-options="region:'center'">
    <div id="tabs" class="easyui-tabs">
        <div title="首页" style="padding:5px;display:block;">
            <p>模板说明：</p>
            <ul>
                <li>主界面使用 easyui1.3.5</li>
                <li>导航树使用 JQuery-zTree-v3.5.15</li>
            </ul>
            <p>特性说明：</p>
            <ul>
                <li>所有弹出框均显示在顶级父窗口</li>
                <li>修改easyui window拖动，移动时显示窗口而不显示虚线框，并限制拖动范围</li>
            </ul>
        </div>
    </div>
</div>

<!-- 用于弹出框 -->
<div id="parent_win"></div>
<!-- tab 右击菜单 -->
<div id="mm" class="easyui-menu" style="width:120px;">
    <div id="tabupdate">刷新</div>
    <div class="menu-sep"></div>
    <div id="close">关闭</div>
    <div id="closeall">全部关闭</div>
    <div id="closeother">关闭其他</div>
    <div class="menu-sep"></div>
    <div id="closeright">关闭右侧</div>
    <div id="closeleft">关闭左侧</div>
    <div class="menu-sep"></div>
    <div id="exit">退出</div>
</div>
</body>
</html>
