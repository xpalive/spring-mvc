<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/resources/common/jsp/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>系统登录</title>
<style type="text/css">
.x-form-item-input {
	width: 220px;
}
</style>
<script type="text/javascript">
	if (self != top) {
		top.location = self.location;
	}
	$(document).ready(function() {
		$('#win').window({
			title : '登录是一种态度!',
			width : 350,
			modal : true,
			resizable : false,
			shadow : false,
			closed : false,
			height : 225,
			closable : false, //去掉关闭按钮 
			cache : false, //进制缓存 
			maximizable : false,
			minimizable : false
		});

	});
	function doCheck() {
		if ($("#adminName").val() == "") {
			errorMsg('请输入用户名！');
			$("#adminName").focus();
			return;
		}
		if ($("#adminPassword").val() == "") {
			errorMsg('请输入密码！');
			$("#adminPassword").focus();
			return;
		}
		if ($("#authCode").val() == "") {
			errorMsg('请输入验证码！');
			$("#authCode").focus();
			return;
		}
		$.ajax({
			type : 'POST',
			url : '${ctx}/admin_login.json',
			data : $('#ff').serialize(),
			dataType : 'json',
			async : false,
			error : function(request) {
				errorMsg("服务器请求异常！");
				refresh();
			},
			success : function(data) {
				if (data.success == true||data.success == 'true') {
						window.location = "${ctx}/admin/index";
				} else {
					errorMsg(data.msg);
					refresh();
				}
			}
		});
	}
	function resets() {
		document.frmLogin.reset();
	}
	function refresh() {
		setTimeout("_refresh()", 700);
	}

	function _refresh() {
		var src = document.getElementById("authImg").src;
		document.getElementById("authImg").src = src + "?now="
				+ new Date().getTime();
	}
</script>
</head>
<body style="height: 90%; width: 90%; overflow: hidden; border: none;">

	<div id="win" iconCls="icon-mima"
		style="padding: 5px; background: #fafafa;">
		<div class="easyui-layout" fit="true">

			<div region="center" border="false"
				style="padding: 10px; background: #fff; border: 1px solid #ccc;">
				<form id="ff" action="${ctx }/admin/login.json" name="frmLogin"
					method="post">
					<table align="center">
						<c:if test="${!empty message}">
							<tr>
								<td colspan="2" align="center"><div class="errormsg">${message}</div></td>
							</tr>
						</c:if>
						<tr>
							<td>用户名：</td>
							<td align="left"><input
								class="easyui-validatebox x-form-item-input" type="text"
								name="adminName" id="adminName" required="true"/></td>
						</tr>
						<tr>
							<td align="right">密码：</td>
							<td align="left"><input
								class="easyui-validatebox x-form-item-input" type="password"
								name="adminPassword" id="adminPassword" required="true"/></td>
						</tr>
						<tr>
							<td><br>验证码：</td>
							<td align="left"><input style="width: 140px"
								class="easyui-validatebox" type="text" name="authCode"
								id="authCode" required="true" /> <img title="看不清?点击刷新"
								onclick="refresh();" id="authImg" src="${ctx }/authimg" /></td>
						</tr>
					</table>
				</form>
			</div>
			<div region="south" border="false"
				style="margin-top: 2px; text-align: right;">
				<a class="easyui-linkbutton" iconCls="" href="javascript:void(0)"
					onclick="doCheck()">登录</a> <a class="easyui-linkbutton" iconCls=""
					href="javascript:void(0)" onclick="resets()">重置</a> <a
					class="easyui-linkbutton" iconCls="" href="javascript:void(0)"
					onclick="">注册</a>
			</div>

		</div>
	</div>

</body>
</html>
