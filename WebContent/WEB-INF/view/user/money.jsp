<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html>
<head>
<%@ include file="/resources/common/jsp/headlibs.jsp" %>
<link rel="stylesheet" type="text/css" href="${ctx}/resources/common/css/user_base.css">
<link rel="stylesheet" type="text/css" href="${ctx}/resources/common/css/user.css">
<script>
function list() {
	var params = $("#listFrom").serialize();
	$.post('list_money.json',
		params,
		callbackRegister,
		'json'
	).error(function() { $.jBox.tip('服务器异常','error'); });
}
function callbackRegister(objResult) {
	$("#span_total").html(objResult.total);
	var listHtml="";
	var list=objResult.rows;
	if(null!=list&&list.length>0){
		for(var i=0;i<list.length;i++){
			listHtml+="<tr><td  align=\"center\"><input type='checkBox' name='ids' value='"+list[i].id+"'/></td>";
			listHtml+="<td class=\"item\" align=\"center\">"+list[i].inputMoney+"</td>";
			listHtml+="<td class=\"item\" align=\"center\">"+list[i].inputDate+"</td>";
			var inputStatus="是";
			if(list[i].inputStatus==0)
				inputStatus="否";
			if(list[i].inputStatus==1)
				inputStatus="审核通过";
			if(list[i].inputStatus==-1)
				inputStatus="审核不通过";
			listHtml+="<td class=\"item\" align=\"center\">"+inputStatus+"</td>";
			var inputType="提现";
			if(list[i].inputType==0)
				inputType="充值";
			listHtml+="<td class=\"item\" align=\"center\">"+inputType+"</td></tr>";
		}
	}
	$("#list").html(listHtml);
}
$(document).ready(function() {
	 list() ;
});
</script>
<title>充值提现记录</title>
</head>
<body>
<%@ include file="/resources/common/jsp/head.jsp" %>
<div class="w980 mt10 mc">
	<%@ include file="/resources/common/jsp/user_meau.jsp" %>
	<div class="right_big">
 		<div class="b">
                <div class="subject">充值提现记录</div>
                <form id="listFrom">
                <div class="p12">
	     			<table id="tablelist">
                        <tr>
                        	<th width="5%">选择</th>
                            <th width="20%">金额</th>
                            <th width="15%">提交时间</th>
                            <th width="15%">是否审核</th>
                            <th width="15%">类型</th>
                        </tr>
                        <tbody id="list">
	                        <tr>
	                            <td colspan="7" class="item" align="center">没有记录</td>
	                        </tr>
                        </tbody>
                    </table>
                     <%@ include file="/resources/common/jsp/page.jsp" %>
                </div>
                </form>
        </div>
	</div>
</div>
<%@ include file="/resources/common/jsp/foot.jsp" %>
</body>
</html>