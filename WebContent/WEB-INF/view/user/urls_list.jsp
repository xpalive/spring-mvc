<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html>
<head>
<%@ include file="/resources/common/jsp/headlibs.jsp" %>
<link rel="stylesheet" type="text/css" href="${ctx}/resources/common/css/user.css">
<link rel="stylesheet" type="text/css" href="${ctx}/resources/common/css/user_base.css">
<script>
function list() {
	var params = $("#listFrom").serialize();
	$.post('list_urls.json',
		params,
		callbackRegister,
		'json'
	).error(function() { $.jBox.tip('服务器异常','error'); });
}
function callbackRegister(objResult) {
	$("#span_total").html(objResult.total);
	var listHtml="";
	var list=objResult.rows;
	if(null!=list&&list.length>0){
		for(var i=0;i<list.length;i++){
			listHtml+="<tr><td  align=\"center\"><input type='checkBox' name='ids' value='"+list[i].urlId+"'/></td>";
			listHtml+="<td class=\"item\" align=\"center\"><a target='_blank' href='"+list[i].targetUrl+"'>"+list[i].targetUrl+"</a></td>";
			listHtml+="<td class=\"item\" align=\"center\">"+list[i].currentPoint+"</td>";
			var sourceIsJump="是";
			if(list[i].sourceIsJump==0)
				sourceIsJump="否";
			listHtml+="<td class=\"item\" align=\"center\">"+sourceIsJump+"</td>";
			var innerIsJump="是";
			if(list[i].innerIsJump==0)
				innerIsJump="否";
			listHtml+="<td class=\"item\" align=\"center\">"+innerIsJump+"</td>";
			var passIs="是";
			if(list[i].passIs==0)
				passIs="否";
			listHtml+="<td align=\"center\">"+passIs+"</td>";
			
			listHtml+="<td align=\"center\"><a href='javaScript:editPoint("+list[i].urlId+")'>编辑</a>";
			listHtml+="<a href='javaScript:addPoint("+list[i].urlId+")'>加积分</a></td></tr>";
		}
	}
	$("#list").html(listHtml);
}
$(document).ready(function() {
	 list() ;
});

function delUrl() {
	var obj = document.getElementsByName('ids');
    var count=obj.length;
    var j=0;
    for(var i=0;i<count;i++)
    {
        if (obj[i].checked){
            j++;
        }
    }
    if(j==0)
    {    
        alert("至少选择一个任务！！");
        return false;
    }
	var params = $("#listFrom").serialize();
	$.post('delete_urls.json',
		params,
		callbackDelUrl,
		'json'
	).error(function() { $.jBox.tip('服务器异常','error'); });
}
function callbackDelUrl(objResult) {
	if(objResult.success == false) {
		$.jBox.tip(objResult.msg,'error');
	}else{
		$.jBox.tip('删除成功！','success', {closed:function(){location.href="/user/urls_list";}});
	}
}

function addPoint(urlId){
	var html = "<div style='padding:10px;'>输入充入积分数：<input type='text' id='currentPoint' name='currentPoint' />剩余${user.userPoint}积分</div>";
	var submit = function (v, h, f) {
	    if (f.currentPoint == '') {
	        $.jBox.tip("请输入要充入的积分数。", 'error', { focusId: "currentPoint" }); // 关闭设置 yourname 为焦点
	        return false;
	    }
	    var params="currentPoint="+f.currentPoint+"&urlId="+urlId;
	    $.jBox.tip('系统正在处理中，请稍后!','loading');
	    $.post('add_urls_point.json',
		params,
		callbackAddPoint,
		'json'
		).error(function() { $.jBox.tip('服务器异常','error'); });
	    return true;
	};
	$.jBox(html, { title: "你充入多少积分？", submit: submit });
}
function callbackAddPoint(objResult) {
	if(objResult.success == false) {
		$.jBox.tip(objResult.msg,'error');
	}else{
		$.jBox.tip('充积分成功！','success', {closed:function(){location.href="/user/urls_list";}});
	}
}
function editPoint(urlId){
	location.href="/user/urls_edit.html?urlId="+urlId;
}
</script>


</head>
<body>
<%@ include file="/resources/common/jsp/head.jsp" %>
<div class="w980 mt10 mc">
	<%@ include file="/resources/common/jsp/user_meau.jsp" %>
	<div class="right_big">
	    <div class="b">
                <div class="subject">所有任务</div>
                <form id="listFrom">
                <div class="p12">
	     			<table id="tablelist">
                        <tr>
                        	<th width="5%">选择</th>
                            <th width="20%">网址</th>
                            <th width="15%">积分数量</th>
                            <th width="15%">是否有来源</th>
                            <th width="15%">是否内转</th>
                            <th width="15%">是否审核</th>
                            <th width="20%">操作</th>
                        </tr>
                        <tbody id="list">
	                        <tr>
	                            <td colspan="7" class="item" align="center">没有记录</td>
	                        </tr>
                        </tbody>
                    </table>
                    <a href="javascript:delUrl();">批量删除</a>
                    <%@ include file="/resources/common/jsp/page.jsp" %>
                </div>
                </form>
        </div>
       </div>
</div>
<%@ include file="/resources/common/jsp/foot.jsp" %>
</body>
</html>