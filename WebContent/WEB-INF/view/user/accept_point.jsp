<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html>
<head>
<%@ include file="/resources/common/jsp/headlibs.jsp" %>
<link rel="stylesheet" type="text/css" href="${ctx}/resources/common/css/user_base.css">
<link rel="stylesheet" type="text/css" href="${ctx}/resources/common/css/user.css">
<script>
function list() {
	var params = $("#listFrom").serialize();
	$.post('list_accept_point.json',
		params,
		callbackRegister,
		'json'
	).error(function() { $.jBox.tip('服务器异常','error'); });
}
function callbackRegister(objResult) {
	$("#span_total").html(objResult.total);
	var listHtml="";
	var list=objResult.rows;
	if(null!=list&&list.length>0){
		for(var i=0;i<list.length;i++){
			listHtml+="<tr><td  align=\"center\"><input type='checkBox' name='ids' value='"+list[i].id+"'/></td>";
			listHtml+="<td class=\"item\" align=\"center\">"+list[i].chargePoint+"</td>";
			listHtml+="<td class=\"item\" align=\"center\">"+list[i].chargeMoney+"元</td>";
			var chargeType="出售";
			if(list[i].chargeType==1)
				chargeType="购买";
			if(list[i].chargeType==2)
				chargeType="转让";
			listHtml+="<td class=\"item\" align=\"center\">"+chargeType+"</td>";
			listHtml+="<td class=\"item\" align=\"center\">"+list[i].sellDate+"</td>";
			var sellStatus="完成";
			if(list[i].sellStatus==1){
				sellStatus="交易中";
			}
			if(list[i].sellStatus==2){
				sellStatus="交易退回";
			}
			listHtml+="<td class=\"item\" align=\"center\">"+sellStatus+"</td>";
			var operate="";
			if((list[i].chargeType==2)&&(list[i].sellStatus==1)){
				operate="<a href='javascript:acceptPointMove("+list[i].id+");'>接收转让</a>";
			}
			listHtml+="<td class=\"item\" align=\"center\">"+operate+"</td></tr>";
			
		}
	}
	$("#list").html(listHtml);
	var pageInfo="";
	if(objResult.total>10){
		
	}
	
	$("#page_info").html(pageInfo);
}
$(document).ready(function() {
	 list() ;
});
function acceptPointMove(id){
	$.post('accept_point_move.json',
		"id="+id,
		callbackPointMove,
		'json'
	).error(function() { $.jBox.tip('服务器异常','error'); });
}
function callbackPointMove(objResult) {
	if(objResult.success == false) {
		$.jBox.tip(objResult.msg,'error');
	}else{
		$.jBox.tip('接收成功！','success', {closed:function(){location.href="/user/accept_point";}});
	}
}
</script>
<title>积分明细</title>
</head>
<body>
<%@ include file="/resources/common/jsp/head.jsp" %>
<div class="w980 mt10 mc">
	<%@ include file="/resources/common/jsp/user_meau.jsp" %>
	<div class="right_big">
 		<div class="b">
                <div class="subject">积分明细</div>
                <form id="listFrom">
                <div class="p12">
	     			<table id="tablelist">
                        <tr>
                        	<th width="5%">选择</th>
                            <th width="10%">交易积分</th>
                            <th width="15%">交易金额</th>
                            <th width="15%">交易类型</th>
                            <th width="15%">交易时间</th>
                            <th width="15%">交易结果</th>
                            <th width="5%">操作</th>
                        </tr>
                        <tbody id="list">
	                        <tr>
	                            <td colspan="7" class="item" align="center">没有记录</td>
	                        </tr>
                        </tbody>
                    </table>
                   <%@ include file="/resources/common/jsp/page.jsp" %>
                </div>
                </form>
        </div>
	</div>
</div>
<%@ include file="/resources/common/jsp/foot.jsp" %>
</body>
</html>