<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html>
<head>
<%@ include file="/resources/common/jsp/headlibs.jsp" %>
<link rel="stylesheet" type="text/css" href="${ctx}/resources/common/css/user_base.css">
<link rel="stylesheet" type="text/css" href="${ctx}/resources/common/css/user.css">
<link rel="stylesheet" type="text/css" href="${ctx}/resources/common/css/user_reg.css">
<script>
$(document).ready(function() {
	$.formValidator.initConfig({formID:"regFrom",onError:function(msg){$.jBox.tip(msg, 'error')}});
	$("#inputMoney").formValidator({onShow:"请输入提现金额",onFocus:"请输入正浮点数，例如100.3",onCorrect:"正确"}).inputValidator({min:50,max:${user.userMoney},type:"value",emptyError:"提现金额不能为空",onErrorMin:"提现金额最少50",onErrorMax:"提现金额不能超过可用金额",onError:"提现金额不正确，请检查"}) ;
	$("#inputBank").formValidator({onShow:"请输入收款人帐号",onFocus:"例如：支付宝xxx@163.com 或 工商银行 6222xxxx xxxx xxx",onCorrect:"输入正确"}).inputValidator({min:1,max:32,emptyError:"不能为空",onError:"提现收款人帐号位数不正确，请检查"});
	$("#inputUserName").formValidator({onShow:"输入收款人账户姓名",onFocus:"例如：李某某",onCorrect:"输入正确"}).inputValidator({min:1,max:32,emptyError:"提现收款人姓名不能为空",onError:"提现收款人姓名位数不正确，请检查"});
	$("#authCodeReg").formValidator({onShow:"请输入验证码",onFocus:"4位验证码",onCorrect:"输入正确"}).inputValidator({min:4,max:4,empty:{leftEmpty:false,rightEmpty:false,emptyError:"验证码两边不能有空符号"},onError:"验证码输入位数不正确，请检查"});
});
function registFormCommit() {
	if(!jQuery.formValidator.pageIsValid()) return false;
	$.jBox.tip('系统正在处理中，请稍后!','loading');
	var params = $("#regFrom").serialize();
	$.post('/user/payout.json',
		params,
		callbackChangePassword,
		'json'
	).error(function() { $.jBox.tip('服务器异常','error'); });
}
function callbackChangePassword(objResult) {
	if(objResult.success == false) {
		$.jBox.tip(objResult.msg,'error');
	}else{
		$.jBox.tip('提交成功！','success', {closed:function(){location.href="/user/money";}});
	}
}
function refresh(){
	setTimeout("_refresh()", 700);
}
function _refresh() {
	var src = document.getElementById("authImg").src;
	document.getElementById("authImg").src = src + "?now="
			+ new Date().getTime();
}
</script>
<title>提现</title>
</head>
<body>
<%@ include file="/resources/common/jsp/head.jsp" %>
<div class="w980 mt10 mc">
	<%@ include file="/resources/common/jsp/user_meau.jsp" %>
	<div class="right_big">
 		<div class="b">
             <div class="subject">提现</div>
             <div class="p35">
                <form id="regFrom">
                <ul id="reg">
                	<li><span>可提现金额：</span><em id="userMoney">${user.userMoney}</em>元</li>
                    <li><span>提现金额：</span><input type="text" name="inputMoney"  id="inputMoney"  class="ip w01 c_oldpass" maxlength="16" value="${user.userMoney}" />元&nbsp;&nbsp;&nbsp;<em class="tip" id="inputMoneyTip">请填写提现金额</em></li>
                    <li><span>收款人帐号：</span><input type="text" name="inputBank" id="inputBank" class="ip w01 c_password" maxlength="16"  /><em class="tip" id="inputBankTip">请输入收款人帐号（支付宝或银行卡卡号）</em></li>
                    <li><span>收款人姓名：</span><input type="text" name="inputUserName" id="inputUserName" class="ip w01 c_repass" maxlength="16" /><em class="tip" id="inputUserNameTip">请输入收款人姓名</em></li>
                    <li><span>验证码：</span><input type="text" name="authCodeReg" id="authCodeReg" class="ip w03 c_code" maxlength="4" required="true" /> 
                       <img onclick="refresh();" id="authImg" src="/authimg" align="absmiddle" /><em class="tip" id="authCodeRegTip">请输入左侧图片上的文字</em></li>
                    <dd><input type="button" value="确认" class="bnt_blue" name="bnt" onClick="registFormCommit();" /> <input type="button" value="取消" onclick="location.href='javascript:history.go(-1)'" class="bnt_blue" /></dd>
                </ul>
                </form>
             
             </div>
         </div>
	</div>
</div>
<%@ include file="/resources/common/jsp/foot.jsp" %>
</body>
</html>