<%@page import="com.model.XiaocaiUser"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html>
<head>
<%@ include file="/resources/common/jsp/headlibs.jsp" %>
<link rel="stylesheet" type="text/css" href="${ctx}/resources/common/css/user_base.css">
<link rel="stylesheet" type="text/css" href="${ctx}/resources/common/css/user.css">
<link rel="stylesheet" type="text/css" href="${ctx}/resources/common/css/user_reg.css">
<script>
$(document).ready(function() {
	$.formValidator.initConfig({formID:"regFrom",onError:function(msg){$.jBox.tip(msg, 'error')}});
	$("#inputMovePoint").formValidator({onShow:"请输入转移积分数",onFocus:"请输入正浮点数，例如100.3",onCorrect:"正确",empty:false}).inputValidator({min:10000,max:${user.userPoint},type:"value",emptyError:"转移积分数不能为空",onErrorMin:"转移积分数最少10000",onErrorMax:"转移积分数不能大于可用积分",onError:"转移积分数不正确，请检查"}) ;
	$("#inputChargeMoney").formValidator({empty:false,onShow:"请输入转让价格",onFocus:"请输入正浮点数，例如100.3",onCorrect:"正确"}).inputValidator({min:0.1,type:"value",emptyError:"转让价格不能为空",onErrorMin:"转让价格最少0.1",onError:"转让价格不正确，请检查"}) ;
	$("#inputMoveUserName").formValidator({ onShow: "请输入积分接收人的帐号", onfocus: "请注意你输入的email格式，例如:wzmaodong@126.com", onCorrect: "谢谢你的合作，你的email正确" }).regexValidator({ regExp: "email", dataType: "enum", onError: "email格式不正确" }).ajaxValidator({
		dataType : "json",
		async : true,
		url : "/valiadteReg.json?validateCode=2",
		success : function(data){
            if(!data.success) {
            	return true;
            }
			return false;
		},
		error: function(jqXHR, textStatus, errorThrown){$.jBox.tip("服务器没有返回数据，可能服务器忙，请重试"+errorThrown);},
		onError : "此邮箱帐号不存在",
		onWait : "正在对邮箱进行合法性校验，请稍候..."
	});
	$("#authCodeReg").formValidator({onShow:"请输入验证码",onFocus:"4位验证码",onCorrect:"输入正确"}).inputValidator({min:4,max:4,empty:{leftEmpty:false,rightEmpty:false,emptyError:"验证码两边不能有空符号"},onError:"验证码输入位数不正确，请检查"});
});
function registFormCommit() {
	if(!jQuery.formValidator.pageIsValid()) return false;
	$.jBox.tip('系统正在处理中，请稍后!','loading');
	var params = $("#regFrom").serialize();
	$.post('/user/point_move.json',
		params,
		callbackChangePassword,
		'json'
	).error(function() { $.jBox.tip('服务器异常','error'); });
}
function callbackChangePassword(objResult) {
	if(objResult.success == false) {
		$.jBox.tip(objResult.msg,'error');
	}else{
		$.jBox.tip('提交成功！','success', {closed:function(){location.href="/user/point";}});
	}
}
function refresh(){
	setTimeout("_refresh()", 700);
}
function _refresh() {
	var src = document.getElementById("authImg").src;
	document.getElementById("authImg").src = src + "?now="
			+ new Date().getTime();
}
</script>
<title>积分转让</title>
</head>
<body>
<%@ include file="/resources/common/jsp/head.jsp" %>
<div class="w980 mt10 mc">
	<%@ include file="/resources/common/jsp/user_meau.jsp" %>
	<div class="right_big">
 		<div class="b">
             <div class="subject">积分转让</div>
             <div class="p35">
                <form id="regFrom">
                <ul id="reg">
                	<li><span>可用积分：</span><fmt:formatNumber value="${user.userPoint}" pattern="#.000"/></li>
                    <li><span>转让积分数：</span><input type="text" name="inputMovePoint" id="inputMovePoint" class="ip w01 c_password" maxlength="16" value="<fmt:formatNumber value="${user.userPoint}" pattern="#.000"/>" /><em class="tip" id="inputMovePointTip">请输入转移的积分数</em></li>
                    <li><span>转让价格：</span><input type="text" name="inputChargeMoney" id="inputChargeMoney" class="ip w01 c_password" maxlength="16" value="0" />元<em class="tip" id="inputChargeMoneyTip">请输入转移给对方，对方应付的金额</em></li>
                    <li><span>接收积分帐号：</span><input type="text" name="inputMoveUserName" id="inputMoveUserName" class="ip w01 c_password" maxlength="16"  /><em class="tip" id="inputMoveUserNameTip">请输入接收积分的帐号</em></li>
                    <li><span>验证码：</span><input type="text" name="authCodeReg" id="authCodeReg" class="ip w03 c_code" maxlength="4" required="true" /> 
                       <img onclick="refresh();" id="authImg" src="/authimg" align="absmiddle" /><em class="tip" id="authCodeRegTip">请输入左侧图片上的文字</em></li>
                    <% XiaocaiUser xiaocaiUser=(XiaocaiUser)session.getAttribute("CURRENT_USER");%>
                    <%if(xiaocaiUser.getUserLevel()>0){ %>
                    	<dd><input type="button" value="确认" class="bnt_blue" name="bnt" onClick="registFormCommit();" /> <input type="button" value="取消" onclick="location.href='javascript:history.go(-1)'" class="bnt_blue" /></dd>
                	<%}else{ %>
                		<li><span>注：</span>只有代理才可进行积分转移</li>
                	<%}%>
                </ul>
                </form>
             
             </div>
         </div>
	</div>
</div>
<%@ include file="/resources/common/jsp/foot.jsp" %>
</body>
</html>