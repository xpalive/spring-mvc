<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html>
<head>
<%@ include file="/resources/common/jsp/headlibs.jsp" %>
<link rel="stylesheet" type="text/css" href="${ctx}/resources/common/css/user_base.css">
<link rel="stylesheet" type="text/css" href="${ctx}/resources/common/css/user.css">
<link rel="stylesheet" type="text/css" href="${ctx}/resources/common/css/user_reg.css">
<script>
$(document).ready(function() {
	$.formValidator.initConfig({formID:"regFrom",onError:function(msg){$.jBox.tip(msg, 'error')}});
	$("#inputMoney").formValidator({onShow:"请输入充值金额",onFocus:"请输入正浮点数，例如100.3",onCorrect:"输入正确"}).inputValidator({min:1,max:32,emptyError:"充值金额不能为空",onError:"充值金额不正确，请检查"}).regexValidator({ regExp: "decmal1", dataType: "enum", onError: "正浮点数格式不正确" }) ;
	$("#inputBank").formValidator({onShow:"请输入充值帐号",onFocus:"例如：支付宝xxx@163.com 或 工商银行 6222xxxx xxxx xxx",onCorrect:"输入正确"}).inputValidator({min:1,max:32,emptyError:"不能为空",onError:"充值帐号位数不正确，请检查"});
	$("#inputUserName").formValidator({onShow:"再次输入充值账户姓名",onFocus:"例如：李某某",onCorrect:"输入正确"}).inputValidator({min:1,max:32,emptyError:"充值账户姓名不能为空",onError:"充值账户姓名位数不正确，请检查"});
	$("#authCodeReg").formValidator({onShow:"请输入验证码",onFocus:"4位验证码",onCorrect:"输入正确"}).inputValidator({min:4,max:4,empty:{leftEmpty:false,rightEmpty:false,emptyError:"验证码两边不能有空符号"},onError:"验证码输入位数不正确，请检查"});
});
function registFormCommit() {
	if(!jQuery.formValidator.pageIsValid()) return false;
	$.jBox.tip('系统正在处理中，请稍后!','loading');
	var params = $("#regFrom").serialize();
	$.post('/user/pay.json',
		params,
		callbackChangePassword,
		'json'
	).error(function() { $.jBox.tip('服务器异常','error'); });
}
function callbackChangePassword(objResult) {
	if(objResult.success == false) {
		$.jBox.tip(objResult.msg,'error');
	}else{
		$.jBox.tip('提交成功！','success', {closed:function(){location.href="/user/money";}});
	}
}
function refresh(){
	setTimeout("_refresh()", 700);
}
function _refresh() {
	var src = document.getElementById("authImg").src;
	document.getElementById("authImg").src = src + "?now="
			+ new Date().getTime();
}
</script>
<title>充值付款</title>
</head>
<body>
<%@ include file="/resources/common/jsp/head.jsp" %>
<div class="w980 mt10 mc">
	<%@ include file="/resources/common/jsp/user_meau.jsp" %>
	<div class="right_big">
 		<div class="b">
             <div class="subject">充值</div>
             <div class="p35">
                <form id="regFrom">
                <ul id="reg">
                    <li><span>第一步：</span>请转账到支付宝帐号：<em style="color:red;">443940556@qq.com(李培燕)</em></li>
                    <li><span>第二步：</span>填写以下表单提交<em style="color:red;">(请勿多次提交)</em></li>
                    <li><span>第三步：</span>联系客服或群管理确认充值</li>
                 </ul>
                <ul id="reg">
                    <li><span>充值金额：</span><input type="text" name="inputMoney"  id="inputMoney"  class="ip w01 c_oldpass" maxlength="16" />元&nbsp;&nbsp;&nbsp;<em class="tip" id="inputMoneyTip">请填写充值金额</em></li>
                    <li><span>充值帐号：</span><input type="text" name="inputBank" id="inputBank" class="ip w01 c_password" maxlength="32"  /><em class="tip" id="inputBankTip">请充值帐号（支付宝或银行卡卡号）</em></li>
                    <li><span>充值人姓名：</span><input type="text" name="inputUserName" id="inputUserName" class="ip w01 c_repass" maxlength="16" /><em class="tip" id="inputUserNameTip">请输入充值人姓名</em></li>
                    <li><span>验证码：</span><input type="text" name="authCodeReg" id="authCodeReg" class="ip w03 c_code" maxlength="4" required="true" /> 
                       <img onclick="refresh();" id="authImg" src="/authimg" align="absmiddle" /><em class="tip" id="authCodeRegTip">请输入左侧图片上的文字</em></li>
                    <dd><input type="button" value="确认" class="bnt_blue" name="bnt" onClick="registFormCommit();" /> <input type="button" value="取消" onclick="location.href='javascript:history.go(-1)'" class="bnt_blue" /></dd>
                </ul>
                </form>
             
             </div>
         </div>
	</div>
</div>
<%@ include file="/resources/common/jsp/foot.jsp" %>
</body>
</html>