<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html>
<head>
<%@ include file="/resources/common/jsp/headlibs.jsp" %>
<link rel="stylesheet" type="text/css" href="${ctx}/resources/common/css/user_base.css">
<link rel="stylesheet" type="text/css" href="${ctx}/resources/common/css/user.css">
<link rel="stylesheet" type="text/css" href="${ctx}/resources/common/css/user_reg.css">
<script>
function registFormCommit() {
	if($("#userLevel").val()>3||$("#userLevel").val()<0){
	 	$.jBox.tip('升级出现错误，联系客服解决','error');
	 	return false;
	}
	$.jBox.tip('系统正在处理中，请稍后!','loading');
	var params = $("#regFrom").serialize();
	$.post('/user/user_level.json',
		params,
		callbackChangePassword,
		'json'
	).error(function() { $.jBox.tip('服务器异常','error'); });
}
function callbackChangePassword(objResult) {
	if(objResult.success == false) {
		$.jBox.tip(objResult.msg,'error');
	}else{
		$.jBox.tip('升级成功！','success', {closed:function(){location.href="/user";}});
	}
}
</script>
<title>修改密码</title>
</head>
<body>
<%@ include file="/resources/common/jsp/head.jsp" %>
<div class="w980 mt10 mc">
	<%@ include file="/resources/common/jsp/user_meau.jsp" %>
	<div class="right_big">
 		<div class="b">
             <div class="subject">用户升级</div>
             <div class="p35">
                <form id="regFrom">
                <ul id="reg">
                	<li><span>当前级别：<input type="hidden" id="userLevel" value="${user.userLevel}" /></span><em class="tip">${user.userLevelName}</em></li>
                    <li><span>选择级别：</span>
                    	<select name="userLevel" id="userLevel">
						  <option value ="1">铜牌代理（开通价格${Level1}元，赠送${Level1_upgrade_gift_point}的积分，1元钱可购买${Point_price_2}积分）</option>
						  <option value ="2">银牌代理（开通价格${Level2}元，赠送${Level2_upgrade_gift_point}的积分，1元钱可购买${Point_price_3}积分）</option>
						  <option value="3">金牌代理（开通价格${Level3}元，赠送${Level3_upgrade_gift_point}的积分，1元钱可购买${Point_price_4}积分）</option>
						</select>
                    </li>
                    <dd><input type="button" value="确认升级" class="bnt_blue" name="bnt" onClick="registFormCommit();" /> <input type="button" value="取消" onclick="location.href='javascript:history.go(-1)'" class="bnt_blue" /></dd>
                </ul>
                </form>
             
             </div>
         </div>
	</div>
</div>
<%@ include file="/resources/common/jsp/foot.jsp" %>
</body>
</html>