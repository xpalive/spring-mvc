<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html>
<head>
<%@ include file="/resources/common/jsp/headlibs.jsp" %>
<link rel="stylesheet" type="text/css" href="${ctx}/resources/common/css/user_base.css">
<link rel="stylesheet" type="text/css" href="${ctx}/resources/common/css/user.css">
<link rel="stylesheet" type="text/css" href="${ctx}/resources/common/css/user_reg.css">
<script>
$(document).ready(function() {
	$.formValidator.initConfig({formID:"regFrom",onError:function(msg){$.jBox.tip(msg, 'error')}});
	$("#oldpass").formValidator({onShow:"请输入旧密码",onFocus:"6-32位数字或字母",onCorrect:"输入正确"}).inputValidator({min:6,max:32,empty:{leftEmpty:false,rightEmpty:false,emptyError:"密码两边不能有空符号"},onError:"密码输入位数不正确，请检查"});
	$("#userPassword").formValidator({onShow:"请输入新密码",onFocus:"6-32位数字或字母",onCorrect:"输入正确"}).inputValidator({min:6,max:32,empty:{leftEmpty:false,rightEmpty:false,emptyError:"密码两边不能有空符号"},onError:"密码输入位数不正确，请检查"});
	$("#repass").formValidator({onShow:"再次输入新密码",onFocus:"输入应与密码一致",onCorrect:"输入正确"}).inputValidator({min:6,max:32,empty:{leftEmpty:false,rightEmpty:false,emptyError:"确认密码两边不能有空符号"},onError:"密码输入位数不正确，请检查"}).compareValidator({desID:"userPassword",operateor:"=",onError:"2次密码输入不一致"});
});
function registFormCommit() {
	if(!jQuery.formValidator.pageIsValid()) return false;
	$.jBox.tip('系统正在处理中，请稍后!','loading');
	var params = $("#regFrom").serialize();
	$.post('/user/re_password.json',
		params,
		callbackChangePassword,
		'json'
	).error(function() { $.jBox.tip('服务器异常','error'); });
}
function callbackChangePassword(objResult) {
	if(objResult.success == false) {
		$.jBox.tip(objResult.msg,'error');
	}else{
		$.jBox.tip('修改成功！','success', {closed:function(){location.href="/user";}});
	}
}
</script>
<title>修改密码</title>
</head>
<body>
<%@ include file="/resources/common/jsp/head.jsp" %>
<div class="w980 mt10 mc">
	<%@ include file="/resources/common/jsp/user_meau.jsp" %>
	<div class="right_big">
 		<div class="b">
             <div class="subject">修改密码</div>
             <div class="p35">
                <form id="regFrom">
                <ul id="reg">
                    <li><span>原密码：</span><input type="password" name="oldpass"  id="oldpass"  class="ip w01 c_oldpass" maxlength="16" /><em class="tip" id="oldpassTip">由6-16个数字、字母、下划线组成</em></li>
                    <li><span>新密码：</span><input type="password" name="userPassword" id="userPassword" class="ip w01 c_password" maxlength="16"  /><em class="tip" id="userPasswordTip">由6-16个数字、字母、下划线组成</em></li>
                    <li><span>再次输入新密码：</span><input type="password" name="repass" id="repass" class="ip w01 c_repass" maxlength="16" /><em class="tip" id="repassTip">请再次输入密码</em></li>
                    <dd><input type="button" value="修改密码" class="bnt_blue" name="bnt" onClick="registFormCommit();" /> <input type="button" value="取消" onclick="location.href='javascript:history.go(-1)'" class="bnt_blue" /></dd>
                </ul>
                </form>
             
             </div>
         </div>
	</div>
</div>
<%@ include file="/resources/common/jsp/foot.jsp" %>
</body>
</html>