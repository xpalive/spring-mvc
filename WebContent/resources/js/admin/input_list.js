var url="";
$(function(){
    var list = $('#list');
    $("#list").datagrid({
        height:$("#body").height()-$('#search_area').height()-5,
        width:$("#body").width(),
        idField:'id',
        url:"input_list.json",
        singleSelect:true,
        nowrap:true,
        fitColumns:true,
        rownumbers:true,
        columns:[[
            {field:'id',title:'id',width:100,halign:"center", align:"left",hidden:true},
            {field:'checkbox',title:'checkbox',width:100,halign:"center", align:"center",checkbox:true},
            {field:'inputType',title:'类型',width:100,halign:"center", align:"center",
                formatter:function(value){
                    if(value == 1){
                        return "提现";
                    }
                    if(value == 0){
                        return "充值";
                    }
                }},
            
            {field:'inputMoney',title:'充值金额',width:100,halign:"center", align:"center"},
            {field:'inputStatus',title:'处理情况',width:100,halign:"center", align:"center",
                formatter:function(value){
                    if(value == 1){
                        return "审核通过";
                    }
                    if(value == 0){
                        return "待审核";
                    }
                    if(value == -1){
                        return "审核不通过";
                    }
                }},
            {field:'inputDate',title:'提交时间',width:100,halign:"center", align:"center"},
            {field:'inputBank',title:'账户',width:100,halign:"center", align:"center"},
            {field:'inputUserName',title:'姓名',width:100,halign:"center", align:"center"},
            {field:'userIdP',title:'用户ID',width:100,halign:"center", align:"center"},
            {field:'inputPassDate',title:'处理时间',width:100,halign:"center", align:"center"}
        ]],
        toolbar:'#list_btn',
        pagination:true,
        onDblClickRow:function(rowIndex, rowData){
            viewDetail(rowData.id);
        }
    });
    //多选
    $("#multi_check").on("click", function(){
        clearSelections(list);
        list.datagrid({singleSelect:false});
        $(this).linkbutton('disable');
        $('#single_check').linkbutton('enable');


    });
    //单选
    $("#single_check").on("click", function(){
        clearSelections(list);
        list.datagrid({singleSelect:true});
        $(this).linkbutton('disable');
        $('#multi_check').linkbutton('enable');
    });
    //查询按钮事件
    $("#search_btn").click(function(){
        list.datagrid('load',{
            "condition['userEmail']":$("#userEmail").val()
        });
    });
    //重置按钮
    $("#reset_btn").click(function(){
        $("#search_form")[0].reset();
    });
    
    
    $("#deal").on("click", function(){
        //$('#list').datagrid('clearSelections');
        var row = $('#list').datagrid('getSelected');
        if (row){
            $('#dlg').dialog('open').dialog('setTitle','充值提现审核');
            $('#fm').form('load',row);
            url = 'deal_input_for_user.json';
        }
    });
});
$(document).ready(function() {
	$.formValidator.initConfig({formID:"fm",onError:function(msg){errorMsg(msg);}});
	$("#userMoney").formValidator({onShow:"金额不能为空",onFocus:"例如1",onCorrect:"输入正确"}).inputValidator({min:1,max:10000,type:"value",emptyError:"文章类型不能为空",onError:"金额不能为空"});
	$("#userPoint").formValidator({onShow:"积分不能为空",onFocus:"例如1",onCorrect:"输入正确"}).inputValidator({min:1,max:10000,type:"value",emptyError:"文章类型不能为空",onError:"积分不能为空"});
});
function pass(isPass){
	$("#isPass").val(isPass);
	if(!jQuery.formValidator.pageIsValid()) return false;
	var params = $("#fm").serialize();
	$.post(url,
		params,
		callbackPass,
		'json'
	).error(function() { errorMsg("服务器异常");});
}
function callbackPass(objResult) {
	if(objResult.success == false) {
		infoMsg(objResult.msg);
	}else{
		infoMsg(objResult.msg);
		$("#dlg").dialog("close");
        $("#list").datagrid("reload");
	}
}