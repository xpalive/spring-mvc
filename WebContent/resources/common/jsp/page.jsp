<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<div class="list_page">
	<a href="javascript:firstPage();">首页</a> <a
		href="javascript:prePage();">上一页</a> <a>第<input id="current_page"
		name="page" type="text" value="1" />页</a> 共<span id="span_total"></span>条
	<input type="hidden" id="pageSize" name="pageSize" value="10" /> <input
		type="hidden" id="pageStart" name="pageStart" value="0" /> <a
		href="javascript:nextPage();">下一页</a> <a href="javascript:lastPage();">尾页</a>
</div>
<script>
	function firstPage() {
		var pageSize = $("#pageSize").val();
		findPage(pageSize, 1);
	}
	function prePage() {
		var pageSize = $("#pageSize").val();
		var current_page = parseInt($("#current_page").val()) ;
		if (current_page <= 1) {
			alert("已经是第一页");
			return;
		}
		findPage(pageSize, current_page- 1);
	}
	function nextPage() {
		var pageSize = $("#pageSize").val();
		var current_page = parseInt($("#current_page").val()) ;
		var total_page = $("#span_total").html() / pageSize;
		if (current_page >= total_page) {
			alert("已经是最后一页");
			return;
		}
		findPage(pageSize, current_page+ 1);
	}
	function lastPage() {
		var pageSize = $("#pageSize").val();
		var current_page = $("#current_page").val();
		var total_page =parseInt(($("#span_total").html() / pageSize)+1);
		findPage(pageSize, total_page);
	}

	function findPage(pageSize, pageCurrent) {
		$("#pageSize").val(pageSize);
		$("#current_page").val(pageCurrent);
		$("#pageStart").val((pageCurrent-1) * pageSize);
		list();
	}
</script>